<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_dictionary".
 *
 * @property int $item_id
 * @property string $item_name
 * @property string $display_name
 * @property int|null $item_value
 * @property string|null $item_image
 * @property string|null $game
 */
class ItemDictionary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_dictionary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'display_name'], 'required'],
            [['item_value'], 'integer'],
            [['item_name', 'display_name', 'item_image', 'game'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'item_name' => 'Item Name',
            'display_name' => 'Display Name',
            'item_value' => 'Item Value',
            'item_image' => 'Item Image',
            'game' => 'Game',
        ];
    }
}
