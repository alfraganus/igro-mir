<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int|null $product_type_id
 * @property int|null $product_category_id
 * @property string|null $title
 * @property string|null $image
 * @property string|null $discount_text
 * @property string|null $discount_product_description
 * @property int|null $quantity
 * @property int|null $item_name
 * @property int|null $is_discounted
 * @property float|null $original_price
 * @property float|null $discounted_price
 * @property int|null $is_active
 *
 * @property ProductCategory $productCategory
 * @property ProductType $productType
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }
    public $avatar;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_type_id', 'product_category_id', 'quantity', 'is_discounted', 'is_active'], 'integer'],
            [['original_price', 'discounted_price'], 'number'],
            [['title','item_name'], 'string', 'max' => 255],
            [['image','discount_text','discount_product_description'], 'string', 'max' => 500],
            [['avatar'], 'file'],
            [['product_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductType::class, 'targetAttribute' => ['product_type_id' => 'id']],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['product_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_type_id' => 'Product Type ID',
            'product_category_id' => 'Product Category ID',
            'title' => 'Display name',
            'item name' => 'Item name (name in the game)',
            'image' => 'Image',
            'quantity' => 'Quantity',
            'is_discounted' => 'Is Discounted',
            'original_price' => 'Original Price',
            'discounted_price' => 'Discounted Price',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * Gets query for [[ProductCategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'product_category_id']);
    }

    /**
     * Gets query for [[ProductType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::class, ['id' => 'product_type_id']);
    }
}
