<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property int $id
 * @property string|null $question
 * @property string|null $content_text
 * @property int|null $content_is_video
 * @property string|null $video_url
 * @property int|null $show_homepage
 * @property int|null $is_active
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_text'], 'string'],
            [['content_is_video', 'show_homepage', 'is_active'], 'integer'],
            [['question', 'video_url'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'content_text' => 'Content',
            'content_is_video' => 'Content Is Video',
            'video_url' => 'Video Url',
            'show_homepage' => 'Show Homepage',
            'is_active' => 'Is Active',
        ];
    }
}
