<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property string|null $payment_token
 * @property string|null $payment_id
 * @property int|null $user_id
 * @property int|null $payment_done
 * @property string|null $response
 * @property float|null $payment_amount
 * @property string|null $datetime
 *
 * @property User $user
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'payment_done', 'payment_type'], 'integer'],
            [['payment_amount'], 'number'],
            [['datetime','response'], 'safe'],
            [['payment_token','payment_id'], 'string', 'max' => 500],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_token' => 'Payment Token',
            'user_id' => 'User ID',
            'payment_done' => 'Payment Done',
            'payment_type' => 'Payment Type',
            'payment_amount' => 'Payment Amount',
            'datetime' => 'Datetime',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
