<?php

namespace app\models;

use yii\base\Model;

class PaymentForm extends Model
{
    public $bank;
    public $sum;
    public $total;
    public $payment_type;
    public $agree;

    public function rules()
    {
        return [
            [['sum', 'total', 'agree'], 'required'],
            [['bank', 'agree','payment_type','total','sum'], 'safe'],
            ['agree', 'boolean'],
            ['agree', 'required', 'requiredValue' => true, 'message' => 'Вы должны согласиться с условиями.'],
        ];
    }
}