<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Products;

/**
 * ProductsSearch represents the model behind the search form of `app\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * {@inheritdoc}
     */

    public $price_start;
    public $price_end;

    public function rules()
    {
        return [
            [['id',  'product_category_id', 'quantity', 'is_discounted', 'is_active'], 'integer'],
            [['title','product_type_id', 'image','price_start','price_end'], 'safe'],
            [['original_price', 'discounted_price'], 'number'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find()
            ->where(['>','quantity',0])
            ->andWhere(['IS NOT', 'item_name', null]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'product_type_id' => $this->product_type_id,
            'product_category_id' => $this->product_category_id,
            'quantity' => $this->quantity,
            'is_discounted' => $this->is_discounted,
            'original_price' => $this->original_price,
            'discounted_price' => $this->discounted_price,
            'is_active' => $this->is_active,
        ]);
        if ($this->price_start && $this->price_end) {
            $query->andFilterWhere(['between', 'original_price', $this->price_start, $this->price_end]);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
