<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales".
 *
 * @property int $id
 * @property int|null $sale_id
 * @property int|null $user_id
 * @property int|null $product_id
 * @property int|null $quantity
 * @property string|null $datetime
 * @property string|null $reserved_nickname
 * @property string|null $reserved_datetime
 * @property int|null $is_claimed
 *
 * @property Products $product
 * @property SalesMain $sale
 */
class Sales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'user_id', 'product_id', 'quantity', 'is_claimed'], 'integer'],
            [['datetime', 'reserved_datetime'], 'safe'],
            [['reserved_nickname'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::class, 'targetAttribute' => ['product_id' => 'id']],
            [['sale_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalesMain::class, 'targetAttribute' => ['sale_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Sale ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'datetime' => 'Datetime',
            'reserved_nickname' => 'Reserved Nickname',
            'reserved_datetime' => 'Reserved Datetime',
            'is_claimed' => 'Is Claimed',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }

    /**
     * Gets query for [[Sale]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(SalesMain::class, ['id' => 'sale_id']);
    }
}
