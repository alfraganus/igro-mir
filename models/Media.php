<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property string|null $image_title
 * @property string|null $image_path
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media';
    }
    public $avatar;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_title'], 'string', 'max' => 255],
            [['image_path'], 'string', 'max' => 500],
            [['avatar'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_title' => 'Image Title',
            'image_path' => 'Image Path',
        ];
    }
}
