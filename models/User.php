<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property int|null $is_active
 * @property string|null $password
 * @property string|null $password_verification
 * @property int|null $is_admin
 * @property string|null $name
 * @property mixed|null $balance
 * @property string|null $auth_key
 * @property string|null $nickname
 * @property string|null $email
 *
 */
class User  extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }
    public $password_initial;
    public $password_confirmation;
    public $confirmAgreement;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password_initial', 'password_confirmation'], 'required','message'=>'Это поле не может быть пустым.'],
            [['is_active','confirmAgreement'], 'integer'],
            [['balance','password_verification'], 'safe'],
            [['nickname', 'auth_key','password','password_confirmation','password_initial'], 'string', 'max' => 255],
            [['password_initial'], 'compare', 'compareAttribute'=>'password_confirmation', 'message'=>"Пароли не совпадают"],
            [['nickname'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 50],
            [['email'], 'unique','message'=>'Пользователь с таким e-mail уже существует'],
            [['email'], 'required','message'=>'Это поле не может быть пустым.'],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Is Active',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'age' => 'Age',
            'payment_stage' => 'Payment Stage',
            'token' => 'Token',
        ];
    }


    public function getAuthKey()
    {
        return $this->auth_key;
    }
    public function setAuthKey()
    {
         $this->auth_key = Yii::$app->security->generateRandomString(50);
    }
    public function setVerificationCode()
    {
        $this->password_verification = Yii::$app->security->generateRandomString(50);
        $this->save(false);
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findByToken($token)
    {
        return static::findOne(['auth_key'=>$token]);
    }

    public static function findByUserId($user_id)
    {
        return static::findOne($user_id);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email'=>$email]);
    }
    public function setToken($length = 32)
    {
        $this->token = Yii::$app->security->generateRandomString($length);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

}