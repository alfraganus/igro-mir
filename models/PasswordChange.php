<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property string|null $password
 * @property string|null $password_verification
 *
 */
class PasswordChange  extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }
    public $new_password;
    public $repeat_password;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password', 'new_password'], 'required','message'=>'Это поле не может быть пустым'],
            [['password', 'new_password','repeat_password','password_verification'], 'string', 'max' => 255],
            ['repeat_password', 'compare', 'compareAttribute'=>'new_password', 'message'=>"Новый пароль и повторный пароль должны совпадать"],
            ['password', 'checkPassword'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'old_password' => 'Is Active',
            'new_password' => 'Name',

        ];
    }

    public function checkPassword($attribute, $params)
    {
        if(Yii::$app->user->isGuest) {
            $user = self::findOne(['password_verification'=>$this->password_verification]);
        } else {
            $user = self::findOne(['id'=>Yii::$app->user->id]);
        }
       /* if (!Yii::$app->security->validatePassword($this->password, $user->password))
            $this->addError($attribute, 'Неверный пароль');*/
    }

}