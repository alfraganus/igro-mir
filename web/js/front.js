$("#sum").keyup(function () {
    updateTotalValue()
})
$("#sum").change(function () {
    updateTotalValue()
})

$(document).ready(function() {
    $("#inventoryAll2").click(function() {
        $(".card-inventory__checkbox:lt(2)").prop("checked", true);
    });
});

$(document).ready(function() {
    $("#inventoryAllCheck").click(function() {
        $(".card-inventory__checkbox:lt(4)").prop("checked", true);
    });

    $("#inventoryUncheck").click(function() {
        $(".card-inventory__checkbox:checked").prop("checked", false);
    });
});

function updateTotalValue() {
    console.log(123)
    var sum = parseFloat(document.getElementById('sum').value);
    if (sum <=50 ) {
        sum = 50
    }
    var selectedPercent = 0;
    var selectedBank = document.querySelector('input[name="bank"]:checked');

    if (selectedBank) {
        var dataPercent = parseFloat(selectedBank.getAttribute('data-percent'));
        selectedPercent = dataPercent;
    }

    var total = sum + (sum * selectedPercent);
   return  document.getElementById('total').value = total.toFixed(2);
}
// updateTotalValue();

function updateFormValue(radio) {
    var selectedValue = null;
    selectedValue = radio.getAttribute('data-id');
    document.getElementById('payment_type').value = selectedValue;
}

$(document).ready(function () {
    setTimeout(function () {
        $("#flash-message").fadeOut("slow");
    }, 1500); // 3000ms = 3s
});

$(".increase").click(function () {
    var $input = $(this).siblings('input.cart-number-input');
    var val = parseInt($input.val());
    var productId = $(this).data('product-id');
    if (val < 5) {
        val += 1;
        $input.val(val);
        return updateCart(val, productId, 'encrease', $input);
        // console.log({ productId, val });
    }
});

$(".decrease").click(function () {
    var $input = $(this).siblings('input.cart-number-input');
    var val = parseInt($input.val());
    var productId = $(this).data('product-id');
    if (val > 1) {
        val -= 1;
        $input.val(val);
        return updateCart(val, productId, 'decrease');
        // console.log({ productId, val });
    }
});

function updateCart(quantity, productId, operation, element = null) {
    $.ajax({
        url: '/shop/cart-quantity', // your yii2 URL structure may differ
        data: {
            quantity: quantity,
            product_id: productId
        },
        type: 'POST',
        success: function (data) {
            if (operation === 'encrease') {
                if (!data['exists']) {
                    element.val(data['max_quantity'])
                } else {
                    element.val(data['current_quantity'])
                }
            }
            $('.overal_sum').text(data['product_sum'])

            console.log(data['product_sum'])
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}


$(document).ready(function () {
    $('.card-cart__delete').click(function () {
        localStorage.setItem('performDeleteAction', 'true');
    });
    var performDeleteAction = localStorage.getItem('performDeleteAction');
    if (performDeleteAction === 'true') {
        // Activate the popup
        $('[data-panel-trigger="cart"]').click();
        // Clear the stored information
        localStorage.removeItem('performDeleteAction');
    }
});

$(document).ready(function () {
    $('.card-product__button').click(function () {
        localStorage.setItem('performAddToCartAction', 'true');
    });
    var performDeleteAction = localStorage.getItem('performAddToCartAction');
    if (performDeleteAction === 'true') {
        // Activate the popup
        $('[data-panel-trigger="cart"]').click();
        // Clear the stored information
        localStorage.removeItem('performAddToCartAction');
    }
});
var ids = [];
var checkboxLimit = 4;
$(document).ready(function () {

    $('.inventory__submit').prop('disabled', true);
    $('.card-inventory__checkbox').change(function () {
        // Check if at least one checkbox is checked
        if ($('.card-inventory__checkbox:checked').length > checkboxLimit) {
            // Uncheck the last checked checkbox
            $(this).prop('checked', false);
            // Show an error message
            alert('Максимум в одном выводе ' + checkboxLimit + ' продукты.');
        }
        if ($('.card-inventory__checkbox:checked').length > 0) {
            $('.inventory__submit').prop('disabled', false);
        } else {
            $('.inventory__submit').prop('disabled', true);
        }
    });

    $('.inventory__submit').click(function () {
        $('#exampleModal').modal('show');
        // Collect checked ids
        $('.card-inventory__checkbox:checked').each(function () {
            var value = $(this).val();
            if (!ids.includes(value)) {
                ids.push(value);
            }
        });

        console.log(ids);
    });
});

$('.assignInvertal').click(function () {
    var nickname = $('#nickname').val();
    $.ajax({
        url: '/shop/assign-inventar', // your yii2 URL structure may differ
        data: {
            products: ids,
            nickname: nickname
        },
        type: 'POST',
        success: function (data) {
            console.log(data)
        },
    });
    $('#pre-sending').hide();
    $('#post-sending').show();

});
$('.check_all').click(function () {
    $('.inventory__submit').prop('disabled', false);
});

$('.uncheck_all').click(function () {
    $('.inventory__submit').prop('disabled', true);
});
$('.close-modal').click(function () {
    $('.modal').modal('hide');
});
