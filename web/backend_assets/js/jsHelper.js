$(document).ready(function() { //
    $("#is_discounted").change(function() {
        if(this.checked) {
            $("#discounted_price").show();
            $("#original_price").hide();
            $("#title_price").text('Discounted price');

        } else {
            $("#discounted_price").hide();
            $("#original_price").show();
            $("#title_price").text('Original price');
        }
    });
});

$(document).ready(function(){
    toggleInputFields();
    $('#content_is_video').change(function() {
        toggleInputFields();
    });
});

function toggleInputFields() {
    if($('#content_is_video').is(':checked')) {
        console.log('te')
        $('#content_text_field').hide();
        $('#video_url_field').show();
    } else {
        $('#content_text_field').show();
        $('#video_url_field').hide();
    }
}