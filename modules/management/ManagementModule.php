<?php

namespace app\modules\management;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * management module definition class
 */
class ManagementModule extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\management\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->layout = 'main';

        if (!Yii::$app->user->isGuest) {
            {
                $existingUser = User::findOne(Yii::$app->user->id);
                if ($existingUser->is_admin == 0) {
                    Yii::$app->user->logout();
                    return Yii::$app->controller->redirect(Yii::$app->request->referrer);
                }
            }
        }
        parent::init();
        \Yii::$app->user->loginUrl = ['management/auth/login'];
    }
}
