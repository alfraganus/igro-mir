<?php

namespace app\modules\management\services;

use app\models\User;
use Yii;

class UserService
{
    const TEMP_EMAIL = 'g.sulaymonov@evoteka.ru';
    public static function userInfo()
    {
        if (Yii::$app->user->id) {
            return User::findOne(Yii::$app->user->id);
        }
        return null;
    }

    public function sendVerificationEmail($verification, $email,$userName)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken'],
                [
                    'code' => $verification,
                    'userName' => $userName,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'Cheburek shop'])
            ->setTo($email)
            ->setSubject('Сброс пароля для ' . $email)
            ->send();
    }

    public function sendContactForm($content, $email,$userName)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'contact_form'],
                [
                    'content' => $content,
                    'userName' => $userName,
                    'email' => $email,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'почта от пользователя'])
            ->setTo('shop4ebyrek@yandex.ru')
            ->setSubject('почта от ' . $email)
            ->send();
    }
}
