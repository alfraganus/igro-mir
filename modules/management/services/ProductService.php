<?php

namespace app\modules\management\services;

use app\models\Cart;
use app\models\ItemDictionary;
use app\models\ProductCategory;
use app\models\Products;
use app\models\ProductType;
use app\models\Sales;
use app\models\User;
use DateTime;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class ProductService
{
    public static function getCategories()
    {
        return ArrayHelper::map(ProductCategory::find()->all(), 'id', 'title');
    }

    public static function getActiveProducts()
    {
        return Products::find()
            ->where(['is_active' => true])
            ->andWhere(['>', 'quantity', 0])
            ->all();
    }

    public static function getTypes()
    {
        return ArrayHelper::map(ProductType::find()->all(), 'id', 'title');
    }

    public function makeSingleDiscount()
    {
        Products::updateAll(['is_discounted' => false, 'discount_text' => null, 'discount_product_description' => null]);
    }

    public function uploadImage(UploadedFile $image)
    {
        $imageName = sprintf('%d%s.%s', time(), $image->baseName, $image->extension);
        $image->saveAs(sprintf('uploads/%s', $imageName));
        if(!$image) return var_dump(print_r($image->error));
        return $imageName;
    }

    public static function getImagePath($model)
    {
        return sprintf('/uploads/%s', $model->image);
    }

    public static function deleteOldImage($model)
    {
        if(file_exists(sprintf('uploads/%s', $model->image))) {
            unlink(sprintf('uploads/%s', $model->image));
        }
    }

    public static function productQuantityInCart($userId)
    {
        return Cart::find()->where(['user_id' => $userId])->count();
    }

    public static function productListInCart($userId)
    {
        $carts = Cart::findAll(['user_id' => $userId]);
        $result = [];
        foreach ($carts as $cart) {
            $result[] = [
                'cart_id' => $cart->id,
                'product_id' => $cart->product_id,
                'product_name' => $cart->product->title,
                'product_image' => sprintf('/uploads/%s', $cart->product->image),
                'product_discounted' => $cart->product->is_discounted ?? null,
                'original_price' => $cart->product->original_price ?? null,
                'discounted_price' => $cart->product->discounted_price ?? null,
                'product_quantity' => $cart->product->quantity,
                'quantity' => $cart->quantity,
            ];
        }
        return $result;
    }

    public static function updateCartProduct($user_id, $product_id, $quantity)
    {
        $cart = Cart::findOne(['user_id' => $user_id, 'product_id' => $product_id]);
        $cart->quantity = $quantity;
        $cart->save(false);
    }

    public static function productSumPrice()
    {
        $sum = 0;
        foreach (ProductService::productListInCart(Yii::$app->user->id) as $cart) {
            if ($cart['product_discounted']) {
                $sum += $cart['discounted_price'] * $cart['quantity'];
            } else {
                $sum += $cart['original_price'] * $cart['quantity'];
            }
        }
        return $sum;
    }

    public static function showUserBalance()
    {
        $user = User::findOne(Yii::$app->user->id);
        if ($user->balance && $user->balance > 0) {
            return number_format($user->balance);
        }
        return 0;
    }

    public static function checkSession($datetime)
    {
        $productDateTime = (new DateTime($datetime))->modify('+10 minutes');

        $currentDateTime = new DateTime();
        if ($currentDateTime > $productDateTime) {
            return false;
        }
        return true;
    }

    public static function getSaleItems($sale_id)
    {
        $sales = Sales::findAll(['sale_id'=>$sale_id]);
        $result = [];
        foreach ($sales as $sale) {
            $product = Products::findOne($sale->product_id);
            $result[] = [
              'id'=>$sale->id,
              'item_name'=>$product->item_name??null,
              'display_name'=>trim($product->title),
              'quantity'=>$sale->quantity
            ];
        }
        return $result;
    }
}
