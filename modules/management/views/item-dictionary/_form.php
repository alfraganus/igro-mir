<?php

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\ItemDictionary $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="item-dictionary-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=  $form->field($model, 'title')->widget(Select2::classname(), [
        'data' => $products,
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
