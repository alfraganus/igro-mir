<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ItemDictionary $model */

$this->title = 'Create Item Dictionary';
$this->params['breadcrumbs'][] = ['label' => 'Item Dictionaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-dictionary-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
