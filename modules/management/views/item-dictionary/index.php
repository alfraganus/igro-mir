<div class="container">
    <form>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Product Name</th>
                <th>display_name Name</th>
                <th>Quantity</th>
                <th>Is Match</th>
                <th>match update</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($result as $item): ?>
                <tr>
                    <td><?php echo $item['product_name']; ?></td>
                    <td><?php echo $item['display_name']; ?></td>
                    <td><?php echo $item['quantity']; ?></td>
                    <td><?php echo $item['is_match'] ? 'Matched' : 'Not Matched'; ?></td>
                    <td>
                        <?php if (!$item['is_match']): ?>
                            <a href="<?=\yii\helpers\Url::to(['match','product_id'=>$item['product_id']])?>">
                                <button type="button" class="btn btn-primary">Match it</button>
                            </a>
                        <?php else: ?>
                            <a href="<?=\yii\helpers\Url::to(['match','product_id'=>$item['product_id']])?>">
                                <button type="button" class="btn btn-danger">update</button>
                            </a>
                        <?php endif; ?>
                    </td>

                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </form>
</div>
