<?php

use app\models\ItemDictionary;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\search\ItemDictionarySearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Item Dictionaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-dictionary-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Item Dictionary', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'item_id',
            'item_name',
            'display_name',
            'item_value',
            'item_image',
            //'game',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ItemDictionary $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'item_id' => $model->item_id]);
                 }
            ],
        ],
    ]); ?>


</div>
