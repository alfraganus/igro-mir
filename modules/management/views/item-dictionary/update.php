<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\ItemDictionary $model */

$this->title = 'Update Item Dictionary: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Item Dictionaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="item-dictionary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'products' => $products,

    ]) ?>

</div>
