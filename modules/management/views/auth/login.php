<?php use yii\bootstrap4\ActiveForm;

$form = ActiveForm::begin(); ?>
<div class="container-fluid p-0">
    <div class="row no-gutters">
        <div class="col-lg-4">
            <div class="authentication-page-content p-4 d-flex align-items-center min-vh-100">
                <div class="w-100">
                    <div class="row justify-content-center">
                        <div class="col-lg-9">
                            <div>
                                <div class="text-center">
                                    <div>
                                        <a href="/" class="logo">
                                            <svg class="logo">
                                                <use href="/img/svgSprite.svg#logo__cheburek"></use>
                                            </svg>
                                        </a>
                                    </div>

                                    <h4 class="font-size-18 mt-4">Welcome !</h4>
                                    <p class="text-muted">Sign in to continue to Cheburek!</p>
                                </div>

                                <div class="p-2 mt-5">
                                        <?= $form->field($model, 'username')->textInput(['placeholder' =>'login',['class'=>'input-group-prepend']])->label(false) ?>
                                        <?= $form->field($model, 'password')->textInput(['placeholder' =>'password',['class'=>'input-group-prepend']])->label(false) ?>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customControlInline">
                                        </div>
                                        <div class="mt-4 text-center">
                                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                                        </div>
                                </div>

                                <div class="mt-5 text-center">
                                     <a href="/" class="font-weight-medium text-primary">  </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="authentication-bg"  style="background-image: url('/img/img__hero.webp');background-size: cover">
                <div class="bg-overlay"></div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>