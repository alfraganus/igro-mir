<?php

/** @var \yii\web\View $this */
/** @var string $content */

use app\assets\ManagementAsset;
use yii\helpers\Html;


ManagementAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<body data-topbar="dark" data-layout="horizontal">

<div id="layout-wrapper">

    <?=Yii::$app->controller->renderPartial('/layouts/_header')?>
    <?=Yii::$app->controller->renderPartial('/layouts/_sidebar')?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
            <?= $content ?>
            </div>
        </div>
        <?=Yii::$app->controller->renderPartial('/layouts/_footer')?>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage();
