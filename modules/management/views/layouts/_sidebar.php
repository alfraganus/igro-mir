<?php

use yii\helpers\Url;

?>
<div class="topnav">
    <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">
            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">
                  <!--  <li class="nav-item">
                        <a class="nav-link" href="<?php /*=Url::to(['default/'])*/?>">
                            <i class="ri-dashboard-line mr-2"></i> Dashboard
                        </a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::to(['media/'])?>">
                            <i class="ri-dashboard-line mr-2"></i> Banner
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::to(['product/'])?>">
                            <i class="ri-dashboard-line mr-2"></i> Products
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::to(['item-dictionary/'])?>">
                            <i class="ri-dashboard-line mr-2"></i> Item dictionary
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::to(['product-category/'])?>">
                            <i class="ri-dashboard-line mr-2"></i> Product category
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::to(['product-type/'])?>">
                            <i class="ri-dashboard-line mr-2"></i> Product type
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=Url::to(['faq/'])?>">
                            <i class="ri-dashboard-line mr-2"></i> Faq
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
