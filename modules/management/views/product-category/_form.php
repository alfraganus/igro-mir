<?php

use app\modules\management\services\ProductService;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="col-md-12">
        <br>
        <?= $form->field($model, 'avatar')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' => array(
                'initialPreview' => isset($model->image) ? ProductService::getImagePath($model): array(),
                'initialPreviewAsData' => true,
                'initialPreviewDownloadUrl' =>isset($model->image) ? ProductService::getImagePath($model): array(),
                'showUpload' => false,
                'overwriteInitial' => true,
                'showRemove' => false,
                'showClose' => false,
                'fileActionSettings' => array(
                    'showRemove' => false
                ),
            ),
        ]); ?>
    </div>

    <?= $form->field($model, 'is_active')->dropDownList([
            1=>'Active', 0=>'InActive'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
