<?php

use app\modules\management\services\ProductService;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Media $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="media-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image_title')->textInput(['maxlength' => true]) ?>

    <div class="row">

        <div class="col-md-12">
            <br>
            <?= $form->field($model, 'avatar')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreview' => isset($model->image_path) ? sprintf('/uploads/%s', $model->image_path):[],
                    'initialPreviewAsData' => true,
                    'initialPreviewDownloadUrl' =>isset($model->image_path) ? sprintf('/uploads/%s', $model->image_path):[],
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'showRemove' => false,
                    'showClose' => false,
                    'fileActionSettings' => [
                        'showRemove' => false
                    ],
                ],
            ]); ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
