<?php

use app\models\Media;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Media';
$this->params['breadcrumbs'][] = $this->title;
$dbImage = Media::find()->count();
?>
<div class="media-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($dbImage < 1): ?>
    <p>
        <?= Html::a('Create Media', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img(sprintf('/uploads/%s', $model->image_path),
                        ['width' => '90px', 'height' => '70px', 'border-radius' => '50%']);
                },
            ],
            'image_title',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Media $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>


</div>
