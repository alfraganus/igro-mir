<?php

use kartik\checkbox\CheckboxX;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Faq $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'content_is_video')->checkbox(['rows' => 6, 'id' => 'content_is_video']) ?>
        </div>
        <div class="col-md-10">
            <?= $form->field($model, 'content_text')->textarea(['rows' => 6, 'id' => 'content_text_field']) ?>
            <?= $form->field($model, 'video_url')->textInput(['maxlength' => true, 'id' => 'video_url_field','display'=>'none','placeholder'=>'Link for youtube'])->label(false) ?>
        </div>
    </div>





    <?= $form->field($model, 'show_homepage')->dropDownList([
        0 => 'Show only in FAQ page',
        1 => 'Show both in homepage and FAQ page',
    ]) ?>
    <?= $form->field($model, 'is_active')->dropDownList([
        1 => 'Active', 0 => 'InActive'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
   $('#content_is_video').change(function() {
       if(this.checked) {
           $('#video_url_field').show();
           $('#content_text_field').hide();
       } else {
            $('#video_url_field').hide();
            $('#content_text_field').show();
       }
         
          
       console.log(this)
     
    });

 
JS;
$this->registerJs($script);
?>

