<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img(\app\modules\management\services\ProductService::getImagePath($model),
                        ['width' => '100px', 'height' => '70px', 'border-radius' => '50%']);
                },
            ],
            'title',
            [
                'attribute' => 'product_type_id',
                'value' => function ($model) {
                    return $model->productType ? $model->productType->title : null;
                },
            ],
            [
                'attribute' => 'product_category_id',
                'value' => function ($model) {
                    return $model->productCategory ? $model->productCategory->title : null;
                },
            ],
            'quantity',
            'is_discounted:boolean',
            [
                'attribute' => 'original_price',
                'value' => function ($model) {
                    return number_format($model->original_price);
                },
            ],
            [
                'attribute' => 'discounted_price',
                'value' => function ($model) {
                    return number_format($model->discounted_price);
                },
            ],
            'discount_text',
            'discount_product_description',
            'is_active:boolean',
        ],
    ]) ?>

</div>
