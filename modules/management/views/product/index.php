<?php

use app\models\Products;
use yii\bootstrap5\LinkPager;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Products', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => false,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            if ($model->is_discounted) {
                return ['class' => 'alert alert-success'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'image',
                'format' => 'html',
                'value' => function ($model) {
                    return Html::img(\app\modules\management\services\ProductService::getImagePath($model),
                        ['width' => '90px', 'height' => '70px', 'border-radius' => '50%']);
                },
            ],
            'title',
            [
                'attribute' => 'product_type_id',
                'value' => function ($model) {
                    return $model->productType ? $model->productType->title : null;
                },
            ],
            [
                'attribute' => 'product_category_id',
                'value' => function ($model) {
                    return $model->productCategory ? $model->productCategory->title : null;
                },
            ],
            'quantity',
            'is_discounted:boolean',
            [
                'attribute' => 'original_price',
                'value' => function ($model) {
                    return number_format($model->original_price);
                },
            ],
            [
                'attribute' => 'discounted_price',
                'value' => function ($model) {
                    return number_format($model->discounted_price??0.00);
                },
            ],
            'is_active:boolean',
            'discount_text',
            'discount_product_description',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Products $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],

        ],

    ]); ?>
<?php
echo LinkPager::widget([
    'pagination' => $dataProvider->pagination,
    'options' => ['class' => 'pagination justify-content-center'], // Add Bootstrap 5 class to the pagination container and center alignment
    'linkContainerOptions' => ['class' => 'page-item'], // Add Bootstrap 5 class to each pagination item container
    'linkOptions' => ['class' => 'page-link'], // Add Bootstrap 5 class to each pagination link
    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'], // Add Bootstrap 5 class to disabled pagination links
]);
?>

</div>

<style>
.alert-success {
    background-color:#bfecdf !important ;
}
</style>
