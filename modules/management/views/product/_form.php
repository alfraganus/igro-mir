<?php

use app\modules\management\services\ProductService;
use kartik\checkbox\CheckboxX;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>;

    <div class="row">

        <div class="col-md-12">
            <br>
            <?= $form->field($model, 'avatar')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'initialPreview' => isset($model->image) ? ProductService::getImagePath($model):[],
                    'initialPreviewAsData' => true,
                    'initialPreviewDownloadUrl' =>isset($model->image) ? ProductService::getImagePath($model):[],
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'showRemove' => false,
                    'showClose' => false,
                    'fileActionSettings' => [
                        'showRemove' => false
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'quantity')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'product_type_id')->dropDownList(ProductService::getTypes()) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'product_category_id')->dropDownList(ProductService::getCategories()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'original_price')->textInput() ?>
        </div>
        <div class="col-md-1 mt-3">
            <center>
            <label class="cbx-label" for="s_3">Is discounted</label>
            <?=
             CheckboxX::widget([
                'model' => $model,
                'attribute' => 'is_discounted',
                'pluginOptions' => [
                    'threeState' => false,
                    'size' => 'md'
                ]
            ]);
            ?>
            </center>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'discounted_price')->textInput() ?>
        </div>

        <div class="col-md-6">
             <?= $form->field($model, 'discount_text')->textInput() ?>
        </div>
        <div class="col-md-6">
             <?= $form->field($model, 'discount_product_description')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>