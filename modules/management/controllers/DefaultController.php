<?php

namespace app\modules\management\controllers;

use app\models\User;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `management` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->redirect(['product/']);
//        return $this->render('index');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect(['auth/login']);
    }

}
