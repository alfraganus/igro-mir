<?php

namespace app\modules\management\controllers;

use app\models\ItemDictionary;
use app\models\Products;
use app\models\search\ItemDictionarySearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ItemDictionaryController implements the CRUD actions for ItemDictionary model.
 */
class ItemDictionaryController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ItemDictionary models.
     *
     * @return string
     */
    public function actionIndex()
    {
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $products = Products::find()->all();

        $result = [];
        foreach ($products as $product) {
            $itemDictionary = ItemDictionary::findOne(['display_name' => trim($product->title)]);
            $is_match = (bool)$itemDictionary;

            $result[] = [
                'product_name' => $product->title,
                'product_id' => $product->id,
                'display_name' => $itemDictionary->item_name ?? null,
                'quantity' => $product->quantity,
                'is_match' => $is_match
            ];
        }
        usort($result, function ($a, $b) {
            if ($a['is_match'] && !$b['is_match']) {
                return 1; // $a is true, $b is false, so $b should come first
            } elseif (!$a['is_match'] && $b['is_match']) {
                return -1; // $a is false, $b is true, so $a should come first
            } else {
                return 0; // Both elements have the same 'is_match' value, maintain their original order
            }
        });

        return $this->render('index', [
            'result' => $result,
        ]);
    }

    /**
     * Displays a single ItemDictionary model.
     * @param int $item_id Item ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($item_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($item_id),
        ]);
    }

    /**
     * Creates a new ItemDictionary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ItemDictionary();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'item_id' => $model->item_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ItemDictionary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $item_id Item ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function actionMatch($product_id)
    {
        $model = Products::findOne($product_id);

       if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
           return $this->redirect(['index']);
       }
        $products = ArrayHelper::map(ItemDictionary::find()->all(),'display_name','display_name');
        return $this->render('update', [
            'model' => $model,
            'products' => $products,
        ]);
    }

    /**
     * Deletes an existing ItemDictionary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $item_id Item ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($item_id)
    {
        $this->findModel($item_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ItemDictionary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $item_id Item ID
     * @return ItemDictionary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($item_id)
    {
        if (($model = ItemDictionary::findOne(['item_id' => $item_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
