<?php

namespace app\modules\management\controllers;

use app\models\LoginForm;
use app\models\User;
use app\modules\management\services\UserService;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `management` module
 */
class AuthController extends Controller
{
    public function actionLogin()
    {
        $this->layout = 'auth';
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['management/auth/login']);
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($this->checkUserRole($model->username)->is_admin == 1) {
                $model->login();
                return $this->redirect(['default/']);
            } else {
                $model->addError('username','You are not admin');
            }
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    private function checkUserRole($username)
    {
      return  User::findByEmail($username);
    }
}
