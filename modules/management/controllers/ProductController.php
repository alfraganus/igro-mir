<?php

namespace app\modules\management\controllers;

use app\models\Products;
use app\models\search\ProductsSearch;
use app\modules\management\services\ProductService;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Products model.
 */
class ProductController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Products models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->pagination->pageSize = 60;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Products();
        $productService = Yii::createObject(ProductService::class);
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                if($model->is_discounted) {
                    $productService->makeSingleDiscount();
                }
                $uploadedImage = UploadedFile::getInstance($model, 'avatar');
                if ($uploadedImage) {
                    $imageName = sprintf('%d%s.%s', time(), $uploadedImage->baseName, $uploadedImage->extension);
                    $uploadedImage->saveAs(sprintf('uploads/%s', $imageName));
                    $model->image = $imageName;
                }
                $model->save();
                return $this->redirect(['index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $productService = Yii::createObject(ProductService::class);
        $model = $this->findModel($id);
        if ($this->request->isPost && $model->load($this->request->post())) {
            if($model->is_discounted) {
                $productService->makeSingleDiscount();
            }
            $uploadedImage = UploadedFile::getInstance($model, 'avatar');
            if ($uploadedImage) {
                ProductService::deleteOldImage($model);
                $imageName = sprintf('%d%s.%s', time(), $uploadedImage->baseName, $uploadedImage->extension);
                $uploadedImage->saveAs(sprintf('uploads/%s', $imageName));
                $model->image = $imageName;
            }
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        ProductService::deleteOldImage($model);
        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
