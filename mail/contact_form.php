<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

?>
<div style="background-color: #f6f6f6; padding: 20px;">
    <div style="max-width: 600px; margin: 0 auto;">
        <div style="background: #fff; padding: 30px; text-align: center; border-radius: 5px;">
            <h2 style="color: #333;">почта от <?=$email?>!</h2>
            <p style="font-size: 16px; color: #333; margin-top: 20px;">
                <?=$content?>
            </p>

            <p style="font-size: 16px; color: #999;">
                с наилучшими пожеланиями  <?=$userName?>
            </p>
        </div>
    </div>
</div>
