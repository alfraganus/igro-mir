<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['shop/reset-password', 'token' =>$code]);
?>
<div style="background-color: #f6f6f6; padding: 20px;">
    <div style="max-width: 600px; margin: 0 auto;">
        <div style="background: #fff; padding: 30px; text-align: center; border-radius: 5px;">
            <h2 style="color: #333;">Привет, <?=$userName?>!</h2>
            <p style="font-size: 16px; color: #333; margin-top: 20px;">
                Вы запросили сброс пароля. Нажмите кнопку ниже, чтобы сбросить пароль.
            </p>

            <p style="margin: 40px 0;">
                <?= Html::a('Сброс пароля', $resetLink, ['class' => 'reset-button', 'style' => 'background: #3490dc; border: none; color: #fff; cursor: pointer; font-size: 18px; padding: 10px 20px; text-decoration: none; border-radius: 5px;']) ?>
            </p>

            <p style="font-size: 16px; color: #999;">
                Если вы не запрашивали сброс пароля, никаких дальнейших действий не требуется.
            </p>
        </div>
    </div>
</div>
