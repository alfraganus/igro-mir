<?php

/** @var yii\web\View $this */

/** @var string $content */

use app\assets\AppAsset;
use app\modules\management\services\ProductService;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;
use yii\helpers\Url;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'apple-touch-icon', 'type' => 'image/x-icon', 'href' => '@web/img/favicon/apple-touch-icon.png']);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>

<?php
$script = <<< JS
   function cth(c){document.documentElement.classList.add(c)}
'ontouchstart' in window?cth('touch'):cth('no-touch');
if(typeof InstallTrigger!=='undefined')cth('firefox');
if(/constructor/i.test(window.HTMLElement)||(function(p){return p.toString()==="[object SafariRemoteNotification]"})(!window['safari']||(typeof safari!=='undefined'&&safari.pushNotification)))cth('safari');
if(/*@cc_on!@*/!!document.documentMode)cth('ie');
if(!(!!document.documentMode)&&!!window.StyleMedia)cth('edge');
if(!!window.chrome&&(!!window.chrome.webstore||!!window.chrome.runtime))cth('chrome');
if(~navigator.appVersion.indexOf("Win"))cth('windows');
if(~navigator.appVersion.indexOf("Mac"))cth('osx');
if(~['iPad','iPhone','iPod'].indexOf(navigator.platform))cth('ios');
if(~navigator.appVersion.indexOf("Linux"))cth('linux');

// Добавление 1vh (использование: height: 100vh; height: calc(var(--vh, 1vh) * 100);) для фикса 100vh на мобилках
let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', vh);
window.addEventListener('resize', () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', vh);
});
JS;
$this->registerJs($script);
?>
<?php if (Yii::$app->session->getFlash('custom')) : ?>
    <?= '<script>$(`[data-panel-trigger="cart"]`).click()</script>'; ?>
<?php endif; ?>
<noscript>У вас отключен JavaScript. Это пугает.</noscript>
<div class="page">
    <div class="page__cart-badge">
        <?php if(!Yii::$app->user->isGuest): ?>
            <button class="cart-badge" data-cart-counter="<?=ProductService::productQuantityInCart(Yii::$app->user->id)?>" data-panel-trigger="cart">
                <svg class="cart-badge__icon">
                    <use href="/img/svgSprite.svg#icon__cart"></use>
                </svg>
            </button>
        <?php endif; ?>
    </div>
    <?=Yii::$app->controller->renderPartial('/layouts/_cart')?>
    <div class="page__header">
        <?php
        foreach (Yii::$app->session->getAllFlashes() as $type => $message) : ?>
            <?php if($type != 'custom'): ?>
                <div class="alert alert-<?=$type?>" role="alert"  id="flash-message">
                    <p class="text-center"><?=$message?></p>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
        <!--header logic-->
        <?php if (Yii::$app->user->isGuest): ?>
            <?= Yii::$app->controller->renderPartial('/layouts/unauthorized_header') ?>
        <?php else : ?>
            <?= Yii::$app->controller->renderPartial('/layouts/authorized_header') ?>
        <?php endif; ?>
    </div>

    <?= $content ?>
    <div class="page__footer">
        <footer class="footer">
            <div class="footer__inner">
                <div class="footer__logo">
                    <svg class="logo">
                        <use href="./img/svgSprite.svg#logo__cheburek"></use>
                    </svg>
                </div>
                <nav class="footer__nav"><a class="footer__nav-item" href="<?=Url::to(['shop/products'])?>">Магазин</a><a
                            class="footer__nav-item" href="<?=Url::to(['shop/inventar'])?>">Инвентарь</a><a class="footer__nav-item"
                                                                                                            href="<?=Url::to(['shop/faq'])?>">FAQ</a><a
                            class="footer__nav-item" href="https://vk.com/4ebyrekshop">Инструкция</a></nav>
                <a class="button footer__vk-button" target="_blank" href="https://www.youtube.com/watch?v=VMsHB4MCV_w">
                    <svg class="button__icon">
                        <use href="img/svgSprite.svg#icon__vk"></use>
                    </svg>
                    <span class="button__title">Группа в Вконтакте</span></a>
                <div class="footer__info">ИП Тойкиев Эмиль Алексеевич, <strong>ИНН:</strong> 023103035067, <strong>ОГРНИП:</strong> 323169000054334
                </div>
                <div class="footer__copy"><?=date('Y')?> © <strong>Cheburek</strong></div>
            </div>
        </footer>
    </div>
    <div class="page__darker"></div>
</div>
<!--<script src="/js/bundle.js"></script>-->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
