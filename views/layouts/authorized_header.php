<?php

use app\modules\management\services\ProductService;
use yii\helpers\Url;

?>
<header class="header">
    <div class="header__inner">
        <a href="/">
            <div class="header__logo">
                <svg class="logo">
                    <use href="/img/svgSprite.svg#logo__cheburek"></use>
                </svg>
            </div>
        </a>
        <div class="header__row">
            <div class="header__nav-scroll">
                <nav class="header__nav">
                    <a class="header__nav-item" href="<?=Url::to(['shop/products'])?>">Магазин</a><a
                            class="header__nav-item" href="<?=Url::to(['shop/inventar'])?>">Инвентарь</a><a class="header__nav-item"
                                                                                           href="<?=Url::to(['shop/faq'])?>">FAQ</a><a
                            class="header__nav-item" target="_blank" href="https://www.youtube.com/watch?v=VMsHB4MCV_w">Инструкция</a></nav>
            </div>
        </div>
        <div class="header__user"><a class="wallet header__wallet" href="<?=Url::to(['shop/profile'])?>">
                <svg class="wallet__icon">
                    <use href="/img/svgSprite.svg#icon__wallet"></use>
                </svg>
                <div class="wallet__title"><span><?=ProductService::showUserBalance()?></span> ₽</div>
            </a>
            <div class="header__divider"></div>
            <div class="account header__account"><a class="account__inner" href="<?=Url::to(['shop/profile'])?>">
                    <div class="account__name"><?=Yii::$app->user->identity->nickname??'User'?></div>
                    <div class="account__email"><?=Yii::$app->user->identity->email?></div>
                </a>
                <a data-method="post" href="<?=\yii\helpers\Url::to(['shop/logout'])?>">
                    <button class="account__button">
                    <svg class="account__icon">
                        <use href="/img/svgSprite.svg#icon__logout"></use>
                    </svg>
                </button>
                </a>
            </div>
        </div>
    </div>
</header>
