<?php

use yii\helpers\Url;

?>
<header class="header">
    <div class="header__inner"><a class="header__logo" href="/">
            <svg class="logo">
                <use href="/img/svgSprite.svg#logo__cheburek"></use>
            </svg>
        </a>
        <div class="header__row">
            <div class="header__nav-scroll">
                <nav class="header__nav"><a class="header__nav-item"
                                            href="<?= Url::to(['shop/products']) ?>">Магазин</a><a
                            class="header__nav-item" href="<?=Url::to(['shop/inventar'])?>">Инвентарь</a><a class="header__nav-item"
                                                                                           href="<?=Url::to(['shop/faq'])?>">FAQ</a><a
                            class="header__nav-item" target="_blank" href="https://www.youtube.com/watch?v=VMsHB4MCV_w">Инструкция</a></nav>
            </div>
        </div>
        <div class="header__user"><a class="button" href="<?= \yii\helpers\Url::to(['shop/authorization']) ?>"><span
                        class="button__title">Войти</span></a></div>
    </div>
</header>