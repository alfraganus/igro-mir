<?php

use app\modules\management\services\ProductService;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="page__cart">
    <div class="panel">
        <div class="panel__header">
            <div class="panel__inner">
                <h2 class="panel__title">Корзина</h2>
                <div class="panel__amount">Предметов: <span><?=ProductService::productQuantityInCart(Yii::$app->user->id)?></span></div>
            </div>
            <button class="panel__close">
                <svg class="panel__icon">
                    <use href="/img/svgSprite.svg#icon__close"></use>
                </svg>
            </button>
        </div>
        <div class="panel__scroll">
            <div class="panel__body">
                <ul class="panel__list">
                    <?php $sum = 0; ?>
                    <?php foreach (ProductService::productListInCart(Yii::$app->user->id) as $cart) : ?>
                    <li class="panel__item">
                        <div class="card-cart">
                            <div class="card-cart__header">
                                <div class="card-cart__miniature"><img class="card-cart__img"
                                                                       src="<?=$cart['product_image']?>"
                                                                       alt="Х.СВ.ЕР.ЛИ.Г.АН"/></div>
                                <div class="card-cart__info">
                                    <div class="card-cart__title"><?=$cart['product_name']?></div>
                                    <?php if ($cart['product_discounted']) : ?>
                                        <?php $sum += $cart['discounted_price']*$cart['quantity']; ?>
                                        <div class="card-cart__old-price"><span><?=number_format($cart['original_price']*$cart['quantity'])?></span> ₽</div>
                                        <div class="card-cart__price"><span><?=number_format($cart['discounted_price']*$cart['quantity'])?></span> ₽</div>
                                    <?php else : ?>
                                    <?php $sum += $cart['original_price']*$cart['quantity']; ?>
                                        <div class="card-cart__price"><span><?= number_format($cart['original_price']*$cart['quantity'])?></span> ₽</div>
                                    <?php endif; ?>

                                </div>
                            </div>
                            <div class="card-cart__footer">
                                <filedset class="number card-cart__number" id="fieldset">
                                    <button  data-product-id="<?=$cart['product_id']?>" class="number__button number__button--sub decrease" type="button">−
                                        <!--+icon('minus')(class='number__icon')--></button>
                                    <button data-product-id="<?=$cart['product_id']?>"  class="number__button number__button--add increase" type="button">+
                                        <!--+icon('plus')(class='number__icon')--></button>
                                    <input  class="number__input card-cart__number cart-number-input" type="number" min="1"
                                           value="<?=$cart['quantity']?>" pattern="[0-9]+" />
                                </filedset>
                                <a href="<?=Url::to(['shop/delete-cart','id'=>$cart['cart_id']])?>">
                                    <button class="card-cart__delete">
                                    <svg class="card-cart__icon">
                                        <use href="/img/svgSprite.svg#icon__trash"></use>
                                    </svg>
                                </button>
                                </a>
                            </div>
                        </div>
                    </li>
                    <?php endforeach; ?>
                    <?php if ($sum == 0) : ?>
                        <div class="alert alert-danger" role="alert">
                            Нет продуктов
                        </div>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <?php if ($sum != 0) : ?>
        <div class="panel__footer panel__footer--cart">
            <div class="panel__cart-total">
                <div class="panel__cart-total-title">К оплате:</div>
                <div class="panel__cart-total-price"><span class="overal_sum"><?=$sum?></span> ₽</div>
            </div>
            <div class="panel__cart-actions">
                <a style="text-decoration: none" href="<?=Url::to(['shop/buy'])?>"> <button class="button  button--primary panel__button"><span>Купить</span>
                </button>
                </a>
                <?= Html::a('Очистить', ['shop/delete-all-cart'], [
                    'class' => 'button  button--secondary panel__button',
                    'data' => [
                        'confirm' => 'Все товары из корзины будут удалены, вы уверены?',
                        'method' => 'post',
                    ],
                ]) ?>

            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php
$this->registerJs( <<< EOT_JS_CODE

 $(document).ready(function() {
        $('.card-cart__number').on('change', function() {
            var quantity = $(this).val();
            var productId = $(this).data('product-id');
            console.log('works');
        });
    });

EOT_JS_CODE
);
?>

