<?php

use yii\bootstrap5\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

?>
<div class="page__main">
    <section class="section">
        <div class="container container--wide">
            <h2 class="section__title  section__title--ginger">Магазин
                <div class="section__after">
                    <svg class="section__texture">
                        <use href="/img/svgSprite.svg#img__texture"></use>
                    </svg>
                </div>
            </h2>

            <div class="cols">
                <?php $form = ActiveForm::begin(['method' => 'get']); ?>
                <div class="cols__aside">
                    <div class="filter">
                        <button class="button  button--tertiary filter__trigger"><span
                                    class="button__title">Фильтр</span></button>
                        <div class="filter__section">
                            <div class="filter__title">Цена, ₽</div>
                            <div class="filter__input-range">
                                <div class="filter__row-input">
                                    <?= $form->field($searchModel, 'price_start', [
                                        'inputOptions' => ['placeholder' => 'от', 'id' => 'price_start'],
                                        'template' => "{input}\n{hint}\n{error}"
                                    ]) ?>
                                    <div class="filter__row-input-divider"></div>
                                    <?= $form->field($searchModel, 'price_end', [
                                        'inputOptions' => ['placeholder' => 'до', 'id' => 'price_end'],
                                        'template' => "{input}\n{hint}\n{error}"
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="filter__divider"></div>
                        <br>
                        <div class="filter__section">
                            <div class="filter__title">Тип</div>
                            <?php
                            $typeOptions = [
                                1 => 'Нож',
                                2 => 'Пистолет',
                                3 => 'Сет',
                                4 => 'Питомец',
                            ];
                            $itemRenderer = function ($index, $label, $name, $checked, $value) {
                                $checkedAttribute = $checked ? 'checked' : '';
                                return <<<HTML
                                    <li class="filter__checkbox-item" style="list-style-type: none;padding-bottom: 8px">
                                        <label class="checkbox">
                                            <input class="checkbox__input" id="" type="checkbox" name="$name" value="$value" $checkedAttribute />
                                            <span class="checkbox__fake-input"></span>
                                            <span class="checkbox__label">$label</span>
                                        </label>
                                    </li>
                                    HTML;
                            };
                            echo $form->field($searchModel, 'product_type_id')->checkboxList($typeOptions, ['item' => $itemRenderer])->label(false);
                            ?>
                            </ul>
                        </div>
                        <button type="submit" class="button  button--tertiary filter__button"><span
                                    class="button__title">Применить</span></button>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
                <?php if(empty($dataProvider->models)):?>
                    <div style="height: 60px" class="alert alert-danger" role="alert">
                        Товар не найден
                    </div>
                <?php endif;?>
                <div class="cols__grid">

                    <?php foreach ($dataProvider->models as $item) : ?>
                        <div class="card-product"><img class="card-product__img"
                                                       src="<?= sprintf('/uploads/%s', $item->image) ?>"
                                                       alt="Х.СВ.ЕР.ЛИ.Г.АН">
                            <div class="card-product__info">
                                <div class="card-product__title"><?= $item->title ?></div>
                                <div class="card-product__row">
                                    <?php if ($item->is_discounted): ?>
                                        <div class="card-product__old-price"><span><?= $item->original_price ?></span> ₽
                                        </div>
                                        <div class="card-product__price"><span><?= $item->discounted_price ?></span> ₽
                                        </div>
                                    <?php else : ?>
                                        <div class="card-product__price"><span><?= $item->original_price ?></span> ₽
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <a style="text-decoration: none" href="<?=Url::to(['shop/add-to-cart','product_id'=>$item->id])?>">
                                    <button class="button  button--tertiary card-product__button"><span
                                                class="button__title">В корзину</span></button>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div id="pagination">
            <center>
                <?php
                echo LinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                    'options' => ['class' => 'pagination justify-content-center'], // Add Bootstrap 5 class to the pagination container and center alignment
                    'linkContainerOptions' => ['class' => 'page-item'], // Add Bootstrap 5 class to each pagination item container
                    'linkOptions' => ['class' => 'page-link'], // Add Bootstrap 5 class to each pagination link
                    'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'], // Add Bootstrap 5 class to disabled pagination links
                ]);
                ?>
            </center>
            </div>
        </div>
    </section>
</div>

<style>
    .filter__row-input {
        margin-bottom: 0;
    }
</style>
 