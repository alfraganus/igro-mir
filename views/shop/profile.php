<?php

use app\modules\management\services\ProductService;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>
<div class="page__main">
    <section class="section">
        <div class="container">
            <h2 class="section__title  section__title--violet">Профиль
                <div class="section__after">
                    <svg class="section__texture">
                        <use href="img/svgSprite.svg#img__texture"></use>
                    </svg>
                </div>
            </h2>
            <div class="profile">
                <div class="profile__inner">
                    <div class="profile__user">
                        <div class="profile__name"
                             style="font-size: 18px"><?= Yii::$app->user->identity->nickname ?? 'User' ?></div>
                        <div class="profile__email"><?= Yii::$app->user->identity->email ?></div>
                    </div>
                    <div class="profile__wallet">
                        <div class="wallet">
                            <svg class="wallet__icon">
                                <use href="/img/svgSprite.svg#icon__wallet"></use>
                            </svg>
                            <div class="wallet__title"><span><?= ProductService::showUserBalance() ?></span> ₽</div>
                        </div>
                        <a class="button  button--blue profile__wallet-button" href="<?=Url::to(['payment-list'])?>"><span class="button__title">Пополнить</span></a>
                    </div>
                    <div class="profile__pass">
                        <div class="profile__pass-title"><?= Html::encode($this->title) ?></div>

                        <?php $form = ActiveForm::begin(); ?>

        <p>Смена пароля</p>
                        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Старый пароль'])->label(false)?>
                        <?= $form->field($model, 'new_password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Новый пароль'])->label(false)?>
                        <?= $form->field($model, 'repeat_password')->passwordInput(['class' => 'form-control', 'placeholder' => 'Повторяющийся пароль'])->label(false)?>

                        <?= Html::submitButton('Сохранить', ['class' => 'button button--primary form__button profile__logout']) ?>

                        <?php ActiveForm::end(); ?>

                        <?= Html::a('Выход',Url::to(['/']), ['class' => 'button button--secondary profile__logout']) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<style>
    .form-control {
        height:50px;
        margin-bottom: 20px;
    }
    .form__button {
        height: 45px;
    }
    .help-block {
        color:darkred;
        text-align: center;
        margin: 15px;
    }
</style>