<div class="page__main">
    <section class="section faq">
        <div class="container">
            <h2 class="section__title  section__title--aqua">Вопрос-Ответ<div class="section__after"><svg class="section__texture">
                        <use href="img/svgSprite.svg#img__texture"></use>
                    </svg></div>
            </h2>
            <div class="faq__grid">
                <?php use yii\widgets\ActiveForm;

                foreach ($faq as $faqSingle): ?>
                    <div class="faq__item">
                        <div class="faq__head">
                            <h3 class="faq__title"><?=$faqSingle->question?></h3><button class="faq__trigger">+</button>
                        </div>
                        <div class="faq__body">
                            <div class="faq__inner">
                                <?php if($faqSingle->content_is_video) : ?>
                                    <div class="faq__video"><iframe src="https://www.youtube.com/embed/<?=$faqSingle->video_url?>" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; web-share" allowfullscreen=""></iframe></div>
                                <?php else : ?>
                                    <p class="faq__text"><?=$faqSingle->content_text?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section class="section ask">
        <div class="container">
            <h2 class="section__title  section__title--ginger">Задайте ваш Вопрос<div class="section__after"><svg class="section__texture">
                        <use href="/img/svgSprite.svg#img__texture"></use>
                    </svg></div>
            </h2>
            <?php $form = ActiveForm::begin(['class' => 'ask__form']);?>
            <div class="row">
            <div class="col-md-6">
                <div class="ask__form-col">
                     <?=  $form->field($model, 'name')->textInput(['placeholder' => 'Имя','style' => 'height: 50px'])->label(false); ?>
                    <?=  $form->field($model, 'email')->textInput(['placeholder' => 'почта','style' => 'height: 50px'])->label(false); ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="ask__form-col">
                    <div class="field">
                        <?=  $form->field($model, 'body')->textarea(['style' => 'height: 130px','placeholder' => 'Содержание'])->label(false); ?>
                    </div>
                    <button class="button  button--primary ask__button"><span class="button__title">Отправить</span>
                    </button>
                </div>
            </div>
            </div>
           <?php ActiveForm::end();?>
        </div>
    </section>
</div>

