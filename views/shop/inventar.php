<div class="page__main">
    <section class="section">
        <div class="container container--wide">
            <h2 class="section__title  section__title--ginger">Инвентарь
                <div class="section__after">
                    <svg class="section__texture">
                        <use href="/img/svgSprite.svg#img__texture"></use>
                    </svg>
                </div>
            </h2>
            <div class="cols">
                <div class="cols__aside">
                    <div class="inventory">
                        <div class="inventory__inner">
                            <button class="button  button--tertiary inventory__button check_all" id="inventoryAllCheck"><span
                                        class="button__title">Выбрать все</span></button>

                            <button class="button  button--tertiary inventory__button uncheck_all"
                                    id="inventoryUncheck"><span
                                        class="button__title">Снять выделение</span></button>
                            <button class="button  button--primary inventory__submit">
                                <span class="button__title">Вывести предметы</span>
                            </button>
                        </div>
                    </div>
                </div>
                <?php if(empty($inventars)):?>
                    <div style="height: 60px" class="alert alert-danger" role="alert">
                        Товар не найден
                    </div>
                <?php endif;?>
                <div class="cols__grid">

                    <?php use app\modules\management\services\ProductService;

                    foreach ($inventars as $inventar) : ?>
                    <?php if ($inventar->reserved_datetime && ProductService::checkSession($inventar->reserved_datetime)) : ?>
                            <input class="card-inventory__checkbox" value="<?= $inventar->id ?>" type="checkbox">
                            <div class="card-inventory__inner">
                                <img class="card-inventory__img" src="/uploads/<?= $inventar->product->image ?>"
                                     alt="<?= $inventar->product->image ?>">
                                <div class="card-inventory__info">
                                  <button class="btn btn-info" data-bs-toggle="modal" data-bs-target="#modalLink"> Продолжить вывод</button>
                                </div>
                            </div>
                    <?php else : ?>
                            <label class="card-inventory">
                                <input class="card-inventory__checkbox" value="<?= $inventar->id ?>" type="checkbox">
                                <div class="card-inventory__inner">
                                    <img class="card-inventory__img" src="/uploads/<?= $inventar->product->image ?>"
                                         alt="<?= $inventar->product->image ?>">
                                    <div class="card-inventory__info">
                                        <div class="card-inventory__title"><?= $inventar->product->title ?></div>
                                        <div class="card-inventory__row">
                                            <div class="card-inventory__price">
                                                <span><?= $inventar->product->original_price ?></span> ₽
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </label>
                    <?php endif; ?>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </section>
</div>

<?=Yii::$app->controller->renderPartial('_modal_nickname')?>
<?=Yii::$app->controller->renderPartial('_modal_link')?>




