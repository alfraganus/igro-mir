<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = 'Вход';
?>

<div class="page__main">
    <section class="section">
        <div class="container">
            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form form--login']
            ]); ?>

            <h2 class="form__title"><?= Html::encode($this->title) ?></h2>
            <div class="form__grid">
                <?= $form->field($model, 'username', [
                    'template' => '<div class="field"><label class="field__inner">{input}</label></div>{error}',
                    'inputOptions' => [
                        'class' => 'input field__input',
                        'placeholder' => 'Почта',
                        'pattern' => '[w-\.]+@([w-]+\.)+[w-]{2,4}$'
                    ],
                ]) ?>

                <?= $form->field($model, 'password', [
                    'template' => '<div class="field"><label class="field__inner">{input}<div class="field__show"><svg class="field__icon field__eye"><use href="img/svgSprite.svg#icon__eye"></use></svg><svg class="field__icon field__eye-off"><use href="img/svgSprite.svg#icon__eye--off"></use></svg></div></label></div>{error}',
                    'inputOptions' => [
                        'class' => 'input field__input',
                        'placeholder' => 'Пароль',
                    ],
                ])->passwordInput() ?>

                <div class="field field--flex">
                    <?= $form->field($model, 'rememberMe', [
                        'template' => '<label class="checkbox">{input}<span class="checkbox__fake-input"></span><span class="checkbox__label">{label}</span>{error}</label>',
                    ])->checkbox([
                        'class' => 'checkbox__input',
                    ], false) ?>
                    <?= Html::a('Забыл пароль', ['shop/password-varification'], ['class' => 'form__forget']) ?>
                </div>


                <?= Html::submitButton('Войти', ['class' => 'button button--primary form__button']) ?>
            </div>
            <?= Html::a('Регистрация', ['shop/registration'], ['class' => 'form__link']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </section>
</div>
<style>
    .input:-webkit-autofill, .input:-webkit-autofill:hover, .input:-webkit-autofill:focus {
        -webkit-text-fill-color:none !important
    }
</style>