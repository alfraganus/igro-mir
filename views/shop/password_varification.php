<div class="page__main">
    <section class="section">
        <div class="container">
            <?php use yii\helpers\Html;
            use yii\helpers\Url;
            use yii\widgets\ActiveForm;

            $form = ActiveForm::begin([
                'options' => ['class' => 'form form--forget'],
            ]) ?>

            <h2 class="form__title">Забыл пароль</h2>
            <p class="form__text">Введите свою электронную почту и мы вышлем вам ссылку для сброса пароля</p>

            <div class="form__grid">
                <div class="field">
                    <?= $form->field($model, 'email', [
                        'template' => '<label class="field__inner">{input}</label>',
                        'options' => ['class' => 'field__input'],
                    ])->input('email', ['class' => 'input field__input', 'placeholder' => 'Почта', 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$']) ?>
                </div>

                <?= Html::submitButton('Сбросить пароль', ['class' => 'button button--primary form__button']) ?>
            </div>

            <?= Html::a('Я вспомнил пароль', Url::to(['shop/authorization']), ['class' => 'form__link']) ?>

            <?php ActiveForm::end() ?>
        </div>
    </section>
</div>