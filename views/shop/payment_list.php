<div class="page__main">
    <section class="section">
        <div class="container">
            <div class="payment">
                <div class="payment__section">
                    <div class="payment__balance">
                        <div class="payment__balance-title">Ваш баланс</div>
                        <div class="payment__balance-amount"><span>142</span> ₽</div>
                    </div>
                </div>
                <div class="payment__section">
                    <div class="payment__types">
                        <label class="payment__type">
                            <input class="payment__radio" name="bank" type="radio" data-id="1" data-percent="0.0269" checked onchange="updateFormValue(this)">
                            <div class="payment__type-tile">
                                <img class="payment__type-img" src="/assets/img__bank--tink.jpg" alt="Тинькофф">
                                <div class="payment__type-info">
                                    <div class="payment__type-title">Тинькофф</div>
                                    <div class="payment__type-percent">2,69 %</div>
                                </div>
                            </div>
                        </label>
                        <!--  <label class="payment__type">
                              <input class="payment__radio" name="bank" type="radio" data-id="2" data-percent="0.03" onchange="updateFormValue(this)">
                              <div class="payment__type-tile">
                                  <img class="payment__type-img" src="/img/yookassa.png" alt="Тинькофф" style="height: 28px;width: 36px;margin-right: 10px;margin-left: 10px">
                                  <div class="payment__type-info">
                                      <div class="payment__type-title">ЮKassa</div>
                                      <div class="payment__type-percent">3,00 %</div>
                                  </div>
                              </div>
                          </label>-->
                        <!-- <label class="payment__type">
                             <input class="payment__radio" name="bank" type="radio" data-id="3" data-percent="0.04" onchange="updateFormValue(this)">
                             <div class="payment__type-tile">
                                 <img class="payment__type-img" src="/assets/img__bank--alfa.jpg" alt="Тинькофф">
                                 <div class="payment__type-info">
                                     <div class="payment__type-title">Альфа банк</div>
                                     <div class="payment__type-percent">4,00 %</div>
                                 </div>
                             </div>
                         </label>-->
                    </div>
                </div>

                <div class="payment__section">
                    <?php use yii\bootstrap5\Html;

                    $form = \yii\widgets\ActiveForm::begin(['options' => ['class' => 'payment__form']]); ?>
                    <div class="payment__title">Сумма пополнения</div>
                    <div class="payment__descr">Выберите желаемое количество средств для пополнения.</div>
                    <div class="payment__row">
                        <div class="field">

                            <?= $form->field($model, 'sum')->textInput(['class' => 'input field__input', 'id' => 'sum','value'=>0])->label('<span class="field__label">Сумма, ₽ (без НДС)</span>') ?>
                            <?= $form->field($model, 'payment_type')->hiddenInput(['id' => 'payment_type','value'=>1])->label(false) ?>
                        </div>
                        <div class="field">
                            <?= $form->field($model, 'total')->textInput(['class' => 'input field__input', 'id' => 'total','readonly'=>true])->label('<span class="field__label">Итого с комиссией, ₽ (без НДС)</span>') ?>
                        </div>
                    </div>
                    <div class="payment__block">
                        <div class="payment__subtitle">Популярное</div>
                        <div class="payment__grid">
                            <?= Html::button('100', ['class' => 'payment__button', 'type' => 'button', 'data-amount' => '100,00']) ?>
                            <?= Html::button('250', ['class' => 'payment__button', 'type' => 'button', 'data-amount' => '250,00']) ?>
                            <?= Html::button('500', ['class' => 'payment__button', 'type' => 'button', 'data-amount' => '500,00']) ?>
                            <?= Html::button('1000', ['class' => 'payment__button', 'type' => 'button', 'data-amount' => '1000,00']) ?>
                            <?= Html::button('2500', ['class' => 'payment__button', 'type' => 'button', 'data-amount' => '2500,00']) ?>
                            <?= Html::button('5000', ['class' => 'payment__button', 'type' => 'button', 'data-amount' => '5000,00']) ?>
                        </div>
                    </div>
                    <div class="field">
                        <div class="field">
                            <div class="field">
                                <?= $form->field($model, 'agree', [
                                    'template' => '{input}{label}{error}', // Add {error} placeholder
                                    'options' => ['class' => 'form-check'],
                                ])->checkbox([
                                    'class' => 'form-check-input',
                                    'label' => '<span class="checkbox__label">Я согласен с ' .
                                        \yii\helpers\Html::a('политикой конфиденциальности', \yii\helpers\Url::to(['privacy-policy']), ['class' => 'link', 'target' => '_blank']) .
                                        ' и ' .
                                        \yii\helpers\Html::a('пользовательским соглашением', \yii\helpers\Url::to(['user-agreement']), ['class' => 'link', 'target' => '_blank']) .
                                        '</span>'
                                ])->label(false) ?>

                                <?php if ($model->hasErrors('agree')): ?>
                                    <div class="alert alert-danger"><?= $model->getFirstError('agree') ?></div>
                                <?php endif; ?>
                            </div>

                        </div>


                    </div>
                    <?= \yii\helpers\Html::submitButton('Пополнить', ['class' => 'button  button--primary payment__submit',
                        'data' => [
                            'confirm' => 'вас перенаправляет на страницу оплаты, нажмите ок для действия',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?php \yii\widgets\ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </section>
</div>