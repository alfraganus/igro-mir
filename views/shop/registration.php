<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="page__main">
    <section class="section">
        <div class="container">

            <?php $form = ActiveForm::begin([
                'options' => ['class' => 'form form--registration'],
            ]); ?>
            <h2 class="form__title">Регистрация</h2>
            <div class="form__grid">

                <?= $form->field($model, 'nickname')->textInput(['class' => 'input field__input', 'placeholder' => 'Никнейм'])->label(false) ?>

                <?= $form->field($model, 'email')->input('email', ['class' => 'input field__input', 'placeholder' => 'Почта', 'pattern' => '[w-\.]+@([w-]+\.)+[w-]{2,4}$'])->label(false) ?>

                <?= $form->field($model, 'password_initial')->passwordInput(['class' => 'input field__input', 'placeholder' => 'Пароль', 'id' => 'password'])->label(false) ?>

                <?= $form->field($model, 'password_confirmation')->passwordInput(['class' => 'input field__input', 'placeholder' => 'Повтор пароля'])->label(false) ?>

              <!--  <div class="form__message">
                    <div class="form__message-title">Требования к паролю:</div>
                    <ul class="form__message-list">
                        <li class="form__message-item">Пароли должны совпадать</li>
                        <li class="form__message-item">Минимальная длина пароля — 4 символа</li>
                    </ul>
                </div>
-->
                <?= $form->field($model, 'confirmAgreement', [
                    'template' => '<div class="checkbox">{input}<span class="checkbox__fake-input"></span>{label}{error}</div>',
                ])->checkbox(['class' => 'checkbox__input','required'=>true, 'label' => null], false)->label('Я согласен с <a class="link" href="#">политикой конфиденциальности</a> и <a class="link" href="#">пользовательским соглашением</a>', ['class' => 'checkbox__label'])->error(['class' => 'help-block']) ?>

                <?= Html::submitButton('Зарегистрироваться', ['class' => 'button button--primary form__button']) ?>

            </div>
            <?= Html::a('Восстановить пароль', ['shop/authorization'], ['class' => 'form__link']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </section>
</div>

<style>
    .field__input,.checkbox__input {
        width:100%
    }
    label {
        font-size: 12px !important;
    }
</style>
