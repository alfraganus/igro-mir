<div class="page__main">
    <section class="hero"><img class="hero__image" src="<?=$bannerImageTitle?>" alt="Лучший магазин MM2"></section>
    <section class="section collection">
        <div class="container">
            <h2 class="section__title  section__title--ginger">Наши коллекции
                <div class="section__after">
                    <svg class="section__texture">
                        <use href="img/svgSprite.svg#img__texture"></use>
                    </svg>
                </div>
            </h2>
            <div class="collection__grid">
                <?php use yii\helpers\Url;

                foreach ($categories as $category): ?>
                    <a target="_blank" class="collection__item"
                       href="<?= Url::to(['shop/products', 'product_category_id' => $category->id]) ?>">
                        <img class="collection__image" style="width: 396px;height: 282px"
                             src="/uploads/<?= $category->image ?>" alt="<?= $category->image ?>">
                        <h3 class="collection__title"><?= $category->title ?></h3>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php if ($discountedProduct): ?>
        <section class="discount">
            <div class="container">
                <div class="discount__inner">
                    <h2 class="discount__title"><?= $discountedProduct->discount_text ?></h2>
                    <div class="discount__card"><img class="discount__card-image"
                                                     src="/uploads/<?= $discountedProduct->image ?>"
                                                     alt="<?= $discountedProduct->image ?>">
                        <div class="discount__card-info">
                            <div class="discount__card-title"><?= $discountedProduct->title ?></div>
                            <div class="discount__card-row">
                                <div class="discount__card-old-price"><?= $discountedProduct->original_price ?> ₽</div>
                                <div class="discount__card-price"><?= $discountedProduct->discounted_price ?> ₽</div>
                            </div>
                            <div class="discount__card-actions">
                                <filedset class="number discount__number" data-quantity="data-quantity">
                                    <button class="number__button number__button--sub" type="button">−
                                        <!--+icon('minus')(class='number__icon')--></button>
                                    <button class="number__button number__button--add" type="button">+
                                        <!--+icon('plus')(class='number__icon')--></button>
                                    <input class="number__input discount__number" type="number" min="1" max="1"
                                           value="1"
                                           pattern="[0-9]+"/></filedset>
                                <a style="text-decoration: none"
                                   href="<?= Url::to(['shop/add-to-cart', 'product_id' => $discountedProduct->id]) ?>">
                                    <button class="button discount__button card-product__button"><span
                                                class="button__title">В корзину</span></button>
                                </a>

                            </div>
                            <div class="discount__small"><?= $discountedProduct->discount_product_description ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <section class="section faq">
        <div class="container">
            <h2 class="section__title  section__title--aqua">Вопрос-Ответ
                <div class="section__after">
                    <svg class="section__texture">
                        <use href="img/svgSprite.svg#img__texture"></use>
                    </svg>
                </div>
            </h2>
            <div class="faq__grid">
                <?php foreach ($faq as $faqSingle): ?>
                    <div class="faq__item">
                        <div class="faq__head">
                            <h3 class="faq__title"><?= $faqSingle->question ?></h3>
                            <button class="faq__trigger">+</button>
                        </div>
                        <div class="faq__body">
                            <div class="faq__inner">
                                <?php if ($faqSingle->content_is_video) : ?>
                                    <div class="faq__video">
                                        <iframe src="https://www.youtube.com/embed/<?= $faqSingle->video_url ?>"
                                                frameborder="0"
                                                allow="accelerometer; encrypted-media; gyroscope; web-share"
                                                allowfullscreen=""></iframe>
                                    </div>
                                <?php else : ?>
                                    <p class="faq__text"><?= $faqSingle->content_text ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</div>