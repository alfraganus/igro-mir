<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => 'abs123']);
?>
<div style="background-color: #f6f6f6; padding: 20px;">
    <div style="max-width: 600px; margin: 0 auto;">
        <div style="background: #fff; padding: 30px; text-align: center; border-radius: 5px;">
            <h2 style="color: #333;">Hello, Alfraganus!</h2>
            <p style="font-size: 16px; color: #333; margin-top: 20px;">
                You have requested a password reset. Click the button below to reset your password.
            </p>

            <p style="margin: 40px 0;">
                <?= Html::a('Reset password', $resetLink, ['class' => 'reset-button', 'style' => 'background: #3490dc; border: none; color: #fff; cursor: pointer; font-size: 18px; padding: 10px 20px; text-decoration: none; border-radius: 5px;']) ?>
            </p>

            <p style="font-size: 16px; color: #999;">
                If you did not request a password reset, no further action is required.
            </p>
        </div>
    </div>
</div>
