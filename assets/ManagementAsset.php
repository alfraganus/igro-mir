<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ManagementAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "backend_assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css",
        "backend_assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css",
        "backend_assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css",
        "backend_assets/css/bootstrap.min.css",
        "backend_assets/css/icons.min.css",
        "backend_assets/css/app.min.css",
    ];
    public $js = [
        "https://cdn.jsdelivr.net/npm/vue@2.7.14/dist/vue.js",
        "backend_assets/libs/bootstrap/js/bootstrap.bundle.min.js",
        "backend_assets/libs/metismenu/metisMenu.min.js",
        "backend_assets/libs/simplebar/simplebar.min.js",
        "backend_assets/libs/node-waves/waves.min.js",
        "backend_assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js",
        "backend_assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-us-merc-en.js",
        "backend_assets/libs/datatables.net/js/jquery.dataTables.min.js",
        "backend_assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js",
        "backend_assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js",
        "backend_assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js",
        "backend_assets/js/pages/dashboard.init.js",
        "backend_assets/js/app.js",
        "backend_assets/js/jsHelper.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
