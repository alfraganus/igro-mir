<?php

namespace app\controllers;

use app\components\YooKassa;
use app\models\Payments;
use app\models\Sales;
use app\models\SalesMain;
use app\models\User;
use app\modules\management\services\ProductService;
use Yii;
use yii\db\Expression;
use yii\rest\Controller;

class ApiController extends Controller
{

    public function actionWithdrawal()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $querySales = SalesMain::find()
            ->where(['sale_finished' => false])
            ->andWhere(['>=', new Expression("datetime + INTERVAL 10 MINUTE"), new Expression('NOW()')])
            ->all();
        $result = [];
        foreach ($querySales as $sale) {
            $saleItems = ProductService::getSaleItems($sale->id);
            if ($saleItems) {
                $result[] = [
                    'id' => $sale->id,
                    'nickname' => $sale->nickname,
                    'items' => $saleItems
                ];
            }

        }
        return $result;
    }

    public function actionMakeSaleDone($sale_id)
    {
        $saleMain = SalesMain::findOne($sale_id);
        $saleMain->sale_finished = true;
        $saleMain->save(false);

        Sales::updateAll(['is_claimed' => true], ['sale_id' => $sale_id]);

        return [
            'message' => 'Sale has been finished'
        ];
    }

    public function actionCheckPaymentStatus($PaymentId)
    {
        $TerminalKey = 1686443616955;
        $Password = 'jnlbztau2yfgtvc5';
//        $PaymentId = 3006529955;

        $hash_data = [
            'TerminalKey' => $TerminalKey,
            'PaymentId' => $PaymentId,
            'Password' => $Password,
        ];

        ksort($hash_data);
        $hash = implode('', $hash_data);
        $hash = hash('sha256', $hash);

        $data = [
            "TerminalKey" => $TerminalKey,
            "PaymentId" => $PaymentId,
            "Token" => $hash,
        ];
        $ch = curl_init('https://securepay.tinkoff.ru/v2/GetState');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res, true);

        return $res;
    }

    public function actionYookassaStatus($paymentId)
    {
        $client = new \YooKassa\Client();
        $client->setAuth(216609, 'live_c261Du29vh0rRHelvvKnbp5I5DHEo5fNXPTCiIM8SaI');
        $result = $client->getPaymentInfo($paymentId);
        return  [
            'all'=>$result,
            'bool'=>$result->paid
        ];
    }

    public function actionRefund()
    {
        return (new YooKassa())->makeRefund();
    }
}
