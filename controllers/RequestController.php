<?php

namespace app\controllers;

use app\components\PaymentStatus;
use app\models\Payments;
use app\models\Products;
use app\models\User;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use YooKassa\Client;
use YooKassa\Model\Payment;

class RequestController extends Controller
{
    const PAYMENT_TYPE_TINKOFF = 1;
    const PAYMENT_TYPE_YOOKASSA = 2;


    public function actionConfirmation()
    {
        if (!Yii::$app->user->isGuest) {
            (new PaymentStatus())->checkAllUserPayments(Yii::$app->user->id);
        }
        return $this->redirect(['/']);
    }

  /*  public function actionConfirmation()
    {
        $request = Yii::$app->request->get('token');
        $payment = Payments::findOne(['payment_token' => $request]);
        if ($payment && $payment->payment_done == PaymentStatus::PAYMENT_NOT_CHECKED) {
            if ((new PaymentStatus())->checkPaymentStatus($payment->payment_id, $payment->payment_type, $payment)) {
                $user = User::findOne($payment->user_id);
                $user->balance += $payment->payment_amount;
                $payment->payment_done = PaymentStatus::PAYMENT_IS_SUCCESSFUL;
                $payment->save(false);
                $user->save(false);
                return $this->render('confirmation');
            } else {
                $payment->payment_done = PaymentStatus::PAYMENT_IS_FAILED;
                $payment->save(false);
                return $this->render('fail');
            }
        }
        return $this->redirect(['/']);
    }*/

}
