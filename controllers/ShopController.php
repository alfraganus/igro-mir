<?php

namespace app\controllers;

use app\components\PaymentStatus;
use app\components\Tinkoff;
use app\components\YooKassa;
use app\models\Cart;
use app\models\Faq;
use app\models\Media;
use app\models\PasswordChange;
use app\models\PasswordVerification;
use app\models\PaymentForm;
use app\models\ProductCategory;
use app\models\Products;
use app\models\Sales;
use app\models\SalesMain;
use app\models\search\ProductsSearch;
use app\models\User;
use app\modules\management\services\ProductService;
use app\modules\management\services\UserService;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class ShopController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, $module, $config = [])
    {
        if (!Yii::$app->user->isGuest) {
            (new PaymentStatus())->checkAllUserPayments(Yii::$app->user->id,true);
        }
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionAssignInventar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $postContent = Yii::$app->request->post();
                $saleMain = new SalesMain();
                $saleMain->user_id = Yii::$app->user->id;
                $saleMain->nickname = $postContent['nickname'];
                if($saleMain->save()) {
                    foreach ($postContent['products'] as $productId) {
                        $inventar = Sales::findOne($productId);
                        if ($inventar) {
                            $inventar->sale_id = $saleMain->id;
                            $inventar->reserved_datetime = date('Y-m-d H:i:s');
                            $inventar->is_claimed = false;
                            $inventar->reserved_nickname = $postContent['nickname'];
                            $inventar->save(false);
                        }
                    }
                }
                $transaction->commit();
            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    public function actionProfile()
    {
        $model = new PasswordChange();
        $user = User::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->password = Yii::$app->security->generatePasswordHash($model->new_password);
            $user->save(false);
            Yii::$app->session->setFlash('success', 'пароль был изменен');

            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('profile', [
            'model' => $model
        ]);
    }

    public function actionPasswordVarification()
    {
        $model = new PasswordVerification();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne(['email' => $model->email]);
            if ($user) {
                $user->setVerificationCode();
                $sendEmail = (new UserService())->sendVerificationEmail(
                    $user->password_verification,
                    $user->email,
                    $user->nickname
                );
                if ($sendEmail) {
                    Yii::$app->session->setFlash('success', 'Ссылка для смены почты отправлена на почту');
                    return $this->redirect(Yii::$app->request->referrer);
                }
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->render('password_varification', [
            'model' => $model
        ]);
    }

    public function actionResetPassword($token)
    {
        $model = PasswordChange::findOne(['password_verification' => $token]);
        $user = User::findOne(['password_verification' => $token]);
        if (!$model) {
            return $this->redirect(['authorization']);
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->password = Yii::$app->security->generatePasswordHash($model->new_password);
            $user->save(false);
            Yii::$app->session->setFlash('success', 'пароль был изменен');

            return $this->redirect(Yii::$app->request->referrer);
        }
        $model->password = '';
        return $this->render('reset_password', [
            'model' => $model
        ]);
    }

    public function actionCartQuantity()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {

            $postContent = Yii::$app->request->post();
            $model = Products::findOne($postContent['product_id']);
            if ($postContent['quantity'] > $model->quantity) {
                return [
                    'exists' => false,
                    'max_quantity' => $model->quantity,
                    'message' => 'Such quantity does not exist!'
                ];
            }
            ProductService::updateCartProduct(Yii::$app->user->id, $postContent['product_id'], $postContent['quantity']);
            $sum = 0;
            foreach (ProductService::productListInCart(Yii::$app->user->id) as $cart) {
                if ($cart['product_discounted']) {
                    $sum += $cart['discounted_price'] * $cart['quantity'];
                } else {
                    $sum += $cart['original_price'] * $cart['quantity'];
                }
            }

            return [
                'exists' => true,
                'current_quantity' => $postContent['quantity'],
                'product_sum' => $sum
            ];
        }
    }

    public function actionDeleteAllCart()
    {
        Cart::deleteAll(['user_id' => Yii::$app->user->id]);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionBuy()
    {
        $userBalance = User::findOne(Yii::$app->user->id);
        $productBalance = ProductService::productSumPrice();
        if ($userBalance->balance < $productBalance) {
            Yii::$app->session->setFlash('danger', 'отсутствие баланса!');
            return $this->redirect(Yii::$app->request->referrer);
        }

        foreach (ProductService::productListInCart(Yii::$app->user->id) as $cart) {
            $product = Products::findOne($cart['product_id']);
            if ($product->quantity < $cart['quantity']) {
                Yii::$app->session->set(sprintf('Not enough quantity is available for the product %s!, 
                the some product may have been alreadt in your invertar', $product->title), true);
                return $this->redirect(['shop/']);
            }
            $sales = new Sales();
            $sales->product_id = $cart['product_id'];
            $sales->quantity = $cart['quantity'];
            $sales->user_id = Yii::$app->user->id;
            $sales->save();
            $product->quantity -= $cart['quantity'];
            $product->save(false);
        }
        $userBalance->balance -= $productBalance;
        $userBalance->save(false);
        Cart::deleteAll(['user_id' => Yii::$app->user->id]);
        Yii::$app->session->set('sale_active', true);
        return $this->redirect(['shop/finish-sale']);
    }

    public function actionFinishSale()
    {
        $checkSession = Yii::$app->session->get('sale_active');
        if ($checkSession) {
            Yii::$app->session->remove('sale_active');
            return $this->render('thanks_page');
        }
        return $this->redirect(['shop/products']);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $categories = ProductCategory::find()->all();
        $faq = Faq::findAll(['show_homepage' => true, 'is_active' => true]);
        $discountedProduct = Products::findOne(['is_discounted' => true, 'is_active' => true]);
        $bannerImage = Media::find()->one();
        $bannerImage ? $bannerImageTitle = sprintf('/uploads/%s', $bannerImage->image_path) : $bannerImageTitle = '/assets/img__hero.webp';
        return $this->render('index', [
            'categories' => $categories,
            'faq' => $faq,
            'bannerImage' => $bannerImage,
            'bannerImageTitle' => $bannerImageTitle,
            'discountedProduct' => $discountedProduct,
        ]);
    }


    public function actionProducts()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $categoryId = Yii::$app->request->get('product_category_id');
        if ($categoryId !== null) {
            $dataProvider->query->andFilterWhere(['product_category_id' => $categoryId]);
        }
        $dataProvider->pagination->pageSize = 36;
        return $this->render('products', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionInventar()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['authorization']);
        }
        $userProducts = Sales::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->andWhere(['OR', ['is_claimed' => 0], ['is_claimed' => null]])
            ->all();

        return $this->render('inventar', [
            'inventars' => $userProducts
        ]);
    }

    public function actionFaq()
    {
        $faq = Faq::findAll(['show_homepage' => true, 'is_active' => true]);
        $model = new ContactForm();

        if ($model->load(Yii::$app->request->post()/* && $model->validate()*/)) {
//            return var_dump($model->validate());
            $sendEmail = (new UserService())->sendContactForm(
                $model->body,
                $model->email,
                $model->name
            );
            if ($sendEmail) {
                Yii::$app->session->setFlash('success', 'Письмо было отправлено');
                return $this->redirect(Yii::$app->request->referrer);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('faq', [
            'faq' => $faq,
            'model' => $model,
        ]);
    }

    public function actionAddToCart($product_id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['authorization']);
        }
        $userId = Yii::$app->user->id;
        $cart = Cart::findOne(['user_id' => $userId, 'product_id' => $product_id]);
        $productInfo = Products::findOne($product_id);

        if (!$cart) {
            $cart = new Cart();
            $cart->quantity = 1;
            $cart->user_id = $userId;
            $cart->product_id = $product_id;
        } else {
            $cart->quantity++;
        }
        if ($cart->quantity > $productInfo->quantity) {
            Yii::$app->session->setFlash('danger', 'Продукт имеет недостаточное количество!');
            return $this->redirect(Yii::$app->request->referrer);
        }
        $cart->save();
        Yii::$app->session->setFlash('success', 'Товар добавлен в корзину');
        return $this->redirect(Yii::$app->request->referrer);

    }

    public function actionRegistration()
    {
        $model = new User();
        if ($model->load($this->request->post()) && $model->validate()) {
            $model->is_admin = 0;
            $model->password = Yii::$app->security->generatePasswordHash($model->password_initial);
            $model->setAuthKey();
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Регистрация прошла успешно.');
                return $this->redirect(['authorization']);
            }
        }

        return $this->render('registration', [
            'model' => $model
        ]);
    }

    public function actionAuthorization()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($this->checkUserRole($model->username)->is_admin == 1) {
                $model->addError('username', 'You are admin, create new account as a user!');
            } else {
                $model->login();
                return $this->redirect(['/']);
            }
        }
        $model->password = '';
        return $this->render('authorization', [
            'model' => $model,
        ]);

    }

    private function checkUserRole($username)
    {
        return User::findByEmail($username);
    }

    public function actionDeleteCart($id)
    {
        $cart = Cart::findOne($id);
        $cart->delete();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPaymentList()
    {
        $paymentModel = new PaymentForm();

        if ($paymentModel->load(Yii::$app->request->post()) && $paymentModel->validate()) {
            if ($paymentModel->payment_type == 1) {
                return (new Tinkoff())->createPayment(intval($paymentModel->sum));
            } elseif ($paymentModel->payment_type == 2) {
                return (new YooKassa())->createPayment(intval($paymentModel->sum));
            }
        }
        return $this->render('payment_list', [
            'model' => $paymentModel
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionUser()
    {
        $user = new User();
        $user->auth_key = 'adminabs';
        $user->nickname = 'admin';
        $user->email = 'admin@gmail.com';
        $user->password = Yii::$app->security->generatePasswordHash('admin');
        $user->is_admin = true;
        $user->is_active = true;
        $user->save(false);
        return var_dump($user->errors);
    }


    public function actionPrivacyPolicy()
    {
        return $this->render('privacy_policy');
    }

    public function actionUserAgreement()
    {
        return $this->render('user_agreement');
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
