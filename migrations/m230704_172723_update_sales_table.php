<?php

use yii\db\Migration;

/**
 * Class m230704_172723_update_sales_table
 */
class m230704_172723_update_sales_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sales_main','sale_finished',$this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sales_main','sale_finished');

    }
}
