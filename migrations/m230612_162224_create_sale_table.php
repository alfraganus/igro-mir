<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sale}}`.
 */
class m230612_162224_create_sale_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users','balance',$this->double()->null());
        $this->createTable('{{%sales}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'product_id'=>$this->integer(),
            'quantity'=>$this->integer(),
            'datetime' => $this->dateTime()->defaultExpression('NOW()'),
        ]);
        $this->addForeignKey(
            'fk-sales-product_id',
            'sales',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-sales-user_id',
            'sales',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users','balance');
        $this->dropForeignKey(
            'fk-sales-product_id',
            'cart'
        );
        $this->dropForeignKey(
            'fk-sales-user_id',
            'cart'
        );
        $this->dropTable('{{%sales}}');
    }
}
