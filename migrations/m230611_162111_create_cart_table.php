<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cart}}`.
 */
class m230611_162111_create_cart_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cart}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'user_id' => $this->integer(),
            'quantity' => $this->integer(),
            'datetime' => $this->dateTime()->defaultExpression('NOW()'),
        ]);
        $this->addForeignKey(
            'fk-cart-product_id',
            'cart',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-cart-user_id',
            'cart',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-cart-product_id',
            'cart'
        );
        $this->dropForeignKey(
            'fk-cart-user_id',
            'cart'
        );
        $this->dropTable('{{%cart}}');
    }
}
