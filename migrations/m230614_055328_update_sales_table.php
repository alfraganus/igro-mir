<?php

use yii\db\Migration;

/**
 * Class m230614_055328_update_sales_table
 */
class m230614_055328_update_sales_table extends Migration
{
    //t
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sales','reserved_nickname',$this->string());
        $this->addColumn('sales','reserved_datetime',$this->dateTime());
        $this->addColumn('sales','is_claimed',$this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sales','reserved_nickname');
        $this->dropColumn('sales','reserved_datetime');
        $this->dropColumn('sales','is_claimed');
    }
}
