<?php

use yii\db\Migration;

/**
 * Class m230603_120023_product
 */
class m230603_120023_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'product_type_id'=>$this->integer(),
            'product_category_id'=>$this->integer(),
            'title' => $this->string(255),
            'image' => $this->string(500),
            'quantity' => $this->integer(),
            'is_discounted' => $this->boolean()->null(),
            'original_price' => $this->double()->null(),
            'discounted_price' => $this->double()->null(),
            'is_active'=>$this->boolean()->defaultValue(true)
        ]);
        $this->createIndex(
            'idx-products-product_type_id',
            'products',
            ['product_type_id','product_category_id']
        );
        $this->addForeignKey(
            'fk-products-product_type_id',
            'products',
            'product_type_id',
            'product_type',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-products-product_category_id',
            'products',
            'product_category_id',
            'product_category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-products-product_type_id',
            'post'
        );

        $this->dropForeignKey(
            'fk-products-product_type_id',
            'products'
        );

        $this->dropForeignKey(
            'fk-products-product_category_id',
            'products'
        );
        $this->dropTable('products');
    }
}
