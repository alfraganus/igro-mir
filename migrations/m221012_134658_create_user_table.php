<?php


use yii\db\Migration;

class m221012_134658_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->comment('Номер телефона'),
            'nickname' => $this->string(255)->notNull()->comment('Имя пользователя'),
            'password' => $this->string(255)->notNull()->comment('Parol'),
            'auth_key' => $this->string(500)->notNull()->comment('auth_key'),
            'is_active' => $this->boolean()->notNull()->defaultValue(1)->comment('Активность'),
            'is_admin' => $this->boolean()->notNull()->defaultValue(1)->comment('is admin'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
