<?php

use yii\db\Migration;

/**
 * Class m230603_120629_faq
 */
class m230603_120629_faq extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'question' => $this->string(500),
            'content_text'=>$this->text()->null(),
            'content_is_video'=>$this->boolean()->null(),
            'video_url'=>$this->string(500),
            'show_homepage'=>$this->boolean()->null(),
            'is_active'=>$this->boolean()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('faq');
    }
}
