<?php

use yii\db\Migration;

/**
 * Class m230706_053640_update
 */
class m230706_053640_update extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments','payment_id',$this->string(500)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payments','payment_id');
    }
}
