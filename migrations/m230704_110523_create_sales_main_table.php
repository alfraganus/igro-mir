<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sales_main}}`.
 */
class m230704_110523_create_sales_main_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sales_main}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'nickname'=>$this->string(),
            'datetime' => $this->dateTime()->defaultExpression('NOW()'),
        ]);
        $this->addColumn('sales','sale_id',$this->integer()->after('id'));

        $this->addForeignKey(
            'fk-sales_main-user_id',
            'sales_main',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-sales_main-sale_id',
            'sales',
            'sale_id',
            'sales_main',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-sales_main-user_id',
            'sales_main'
        );
        $this->dropForeignKey(
            'fk-sales_main-sale_id',
            'sales_main'
        );
        $this->dropTable('{{%sales_main}}');
    }
}
