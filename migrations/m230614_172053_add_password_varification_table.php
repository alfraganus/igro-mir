<?php

use yii\db\Migration;

/**
 * Class m230614_172053_add_password_varification_table
 */
class m230614_172053_add_password_varification_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users','password_verification',$this->string(500)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users','password_verification');
    }
}