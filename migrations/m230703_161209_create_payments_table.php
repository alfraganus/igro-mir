<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payments}}`.
 */
class m230703_161209_create_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payments}}', [
            'id' => $this->primaryKey(),
            'payment_token'=>$this->string(500),
            'user_id'=>$this->integer(),
            'payment_done'=>$this->boolean(),
            'payment_type'=>$this->integer(),
            'payment_amount'=>$this->double(),
            'datetime' => $this->dateTime()->defaultExpression('NOW()'),
        ]);
        $this->addForeignKey(
            'fk-payments-user_id',
            'payments',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-payments-user_id',
            'payments'
        );
        $this->dropTable('{{%payments}}');
    }
}
