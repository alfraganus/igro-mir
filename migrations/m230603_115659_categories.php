<?php

use yii\db\Migration;

/**
 * Class m230603_115659_categories
 */
class m230603_115659_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'image' => $this->string(500),
            'is_active'=>$this->boolean()->defaultValue(true)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('categories');
    }
}
