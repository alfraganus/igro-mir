<?php

use yii\db\Migration;

/**
 * Class m230603_115659_categories
 */
class m230603_115669_product_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_type', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'is_active'=>$this->boolean()->defaultValue(true)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_type');
    }
}
