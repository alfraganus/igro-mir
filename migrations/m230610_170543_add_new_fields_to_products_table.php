<?php

use yii\db\Migration;

/**
 * Class m230610_170543_add_new_fields_to_products_table
 */
class m230610_170543_add_new_fields_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products','discount_text',$this->string(500)->null());
        $this->addColumn('products','discount_product_description',$this->string(500)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products','discount_text');
        $this->dropColumn('products','discount_product_description');
    }
}
