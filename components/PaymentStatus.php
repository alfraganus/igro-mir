<?php

namespace app\components;

use app\models\Payments;
use app\models\User;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\httpclient\Client;

class PaymentStatus extends Component
{
    const PAYMENT_NOT_CHECKED = 0;
    const PAYMENT_IS_SUCCESSFUL = 1;
    const PAYMENT_IS_FAILED = 2;
    const PAYMENT_TYPE_TINKOFF = 1;
    const PAYMENT_TYPE_YOOKASSA = 2;


    public function checkAllUserPayments($user_id, $checkAfter15Minutes = false)
    {
        $payments = Payments::find()->where([
            'payment_done' => self::PAYMENT_NOT_CHECKED,
            'user_id' => $user_id
        ])->each();

        $currentDateTime = new \DateTime();

        foreach ($payments as $payment) {
            $paymentDateTime = new \DateTime($payment->datetime);
            $elapsedTime = $currentDateTime->getTimestamp() - $paymentDateTime->getTimestamp();

            if ($checkAfter15Minutes && $elapsedTime < (10 * 60)) {
                // Payment was made less than 15 minutes ago, skip for now
                continue;
            }

            $paymentCheckRequest = $this->checkPaymentStatus($payment->payment_id, $payment->payment_type, $payment);
            if ($paymentCheckRequest['success']) {
                $user = User::findOne($payment->user_id);
                $user->balance += $payment->payment_amount;
                $user->save(false);

                $payment->payment_done = self::PAYMENT_IS_SUCCESSFUL;
            } else {
                $payment->payment_done = self::PAYMENT_IS_FAILED;
            }
            $payment->response = json_encode($paymentCheckRequest['data']);
            $payment->save(false);
        }
    }

    public function checkAllPayments()
    {
        $payments = Payments::find()->where([
            'payment_done' => self::PAYMENT_IS_FAILED
        ])->each();

        foreach ($payments as $payment) {
            $paymentCheckRequest = $this->checkPaymentStatus($payment->payment_id, $payment->payment_type, $payment);
            if ($paymentCheckRequest['success']) {
                $user = User::findOne($payment->user_id);
                $user->balance += $payment->payment_amount;
                $user->save(false);

                $payment->payment_done = self::PAYMENT_IS_SUCCESSFUL;
            } else {
                $payment->payment_done = self::PAYMENT_IS_FAILED;
            }
            $payment->response = json_encode($paymentCheckRequest['data']);
            $payment->save(false);
        }
    }

    public function checkPaymentStatus($paymentId, $payment_type_id, $paymentModel = null)
    {
        if ($payment_type_id == self::PAYMENT_TYPE_YOOKASSA) {
            $client = new \YooKassa\Client();
            $client->setAuth(216609, 'live_c261Du29vh0rRHelvvKnbp5I5DHEo5fNXPTCiIM8SaI');
            $result = $client->getPaymentInfo($paymentId);

            return [
                'success'=>$result->paid,
                'data'=>[
                    'status'=>$result->status,
                    'paid' =>$result->paid,
                    'amount'=>$result->paid,
                ]
            ];
        } elseif ($payment_type_id == self::PAYMENT_TYPE_TINKOFF) {
            $paymentInfo = json_decode($paymentModel->payment_id, true);
            $TerminalKey = $paymentInfo['TerminalKey'];
            $Password = 'jnlbztau2yfgtvc5';
            $PaymentId = $paymentInfo['PaymentId'];

            $hash_data = [
                'TerminalKey' => $TerminalKey,
                'PaymentId' => $PaymentId,
                'Password' => $Password,
            ];

            ksort($hash_data);
            $hash = implode('', $hash_data);
            $hash = hash('sha256', $hash);

            $data = [
                "TerminalKey" => $TerminalKey,
                "PaymentId" => $PaymentId,
                "Token" => $hash,
            ];
            $ch = curl_init('https://securepay.tinkoff.ru/v2/GetState');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $res = curl_exec($ch);
            curl_close($ch);
            $success = false;
            $res = json_decode($res, true);
            if ($res['Status']== 'CONFIRMED') {
                $success = true;
            }
            return [
                'success'=>$success,
                'data'=>[
                    'success'=>$res['Success'],
                    'message'=>$res['Message'],
                    'status'=>$res['Status'],
                    'payment_id'=>$res['PaymentId'],
                ]
            ];
        }
    }

}
