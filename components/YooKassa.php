<?php

namespace app\components;

use app\models\Payments;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\httpclient\Client;

class YooKassa extends Component
{
    public $password;

    const SERVICE_PERCENTAGE = 2.69;
    const PAYMENT_TYPE = 2;

    public function createPayment($amount)
    {
//        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $client = new \YooKassa\Client();
        $client->setAuth(216609, 'live_c261Du29vh0rRHelvvKnbp5I5DHEo5fNXPTCiIM8SaI');
        try {
            $idempotenceKey = uniqid('', true);
            $response = $client->createPayment(
                [
                    'amount' => [
                        'value' => doubleval($amount),
                        'currency' => 'RUB',
                    ],
                    'confirmation' => [
                        'type' => 'redirect',
                        'locale' => 'ru_RU',
                        'return_url' => "https://cheburek-shop.ru/request/confirmation?token=$idempotenceKey",
                    ],
                    'capture' => true,
                    'receipt' => [
                        'customer' => [
                            'phone' => '+79963892233',
                        ],
                        'items' => [
                            [
                                'description' => 'Оплата за чебурек',
                                'quantity' => '1.00',
                                'amount' => [
                                    'value' => doubleval($amount),
                                    'currency' => 'RUB'
                                ],
                                'vat_code' => '2',
                                'payment_mode' => 'full_payment',
                                'payment_subject' => 'commodity',
                            ],

                        ]
                    ]
                ],
                $idempotenceKey
            );
            $this->setPayment(Yii::$app->user->id, $amount, self::PAYMENT_TYPE, $idempotenceKey, $response->id);
            return Yii::$app->controller->redirect($response->getConfirmation()->getConfirmationUrl());
        } catch (\Exception $e) {
            $response = $e;
        }

        if (!empty($response)) {
            echo "<pre>";
            var_dump(print_r($response));
        }
    }

    public function makeRefund()
    {
        $client = new \YooKassa\Client();
        $client->setAuth(216609, 'live_c261Du29vh0rRHelvvKnbp5I5DHEo5fNXPTCiIM8SaI');
        try {
            $response = $client->createRefund(
                [
                    'payment_id' => '2c4f2d2e-000f-5000-a000-1e851af18b8d',
                    'amount' => [
                        'value' => '200.00',
                        'currency' => \YooKassa\Model\CurrencyCode::RUB,
                    ],
                    'sources' => [
                        [
                            'account_id' => '456',
                            'amount' => [
                                'value' => '200.00',
                                'currency' => \YooKassa\Model\CurrencyCode::RUB,
                            ]
                        ],
                    ],
                ],
                uniqid('64bd196eace089.46327022')
            );
            return $response->getStatus();
        } catch (\Exception $e) {
            return $response = $e;
        }
    }

    private function setPayment($user_id, $amount, $payment_type, $token, $payment_id = null)
    {
        $paymentModel = new Payments();
        $paymentModel->payment_token = $token;
        $paymentModel->user_id = $user_id;
        $paymentModel->payment_amount = $amount;
        $paymentModel->payment_type = $payment_type;
        $paymentModel->payment_done = false;
        if ($payment_id) {
            $paymentModel->payment_id = $payment_id;
        }
        $paymentModel->save(false);
    }
}
