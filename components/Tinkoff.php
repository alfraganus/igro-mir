<?php
namespace app\components;

use app\models\Payments;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use yii\httpclient\Client;

class Tinkoff extends Component
{
    public $password;

    CONST SERVICE_PERCENTAGE = 2.69;
    CONST PAYMENT_TYPE = 1;
    public function createPayment( $amount)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $terminalKey = '1686443616955';
        $token =  Yii::$app->security->generateRandomString(150);
        $order_id = time();
        $result = $amount + ($amount * (self::SERVICE_PERCENTAGE / 100));
        $user_id = \Yii::$app->user->id;
        $finalSum = $result * 100;
        $data = [
            'TerminalKey' => $terminalKey,
            'Amount' =>$finalSum,
            'OrderId' => $order_id,
            'SuccessURL' => "https://cheburek-shop.ru/request/confirmation?token=$token",
            'PayType' => 'O',
        ];

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl('https://securepay.tinkoff.ru/v2/Init')
            ->addHeaders(['Content-Type' => 'application/json'])
            ->setContent(Json::encode($data, JSON_UNESCAPED_UNICODE))
            ->setOptions([
                'sslVerifyPeer' => false,
            ])
            ->send();

        if ($response->isOk) {
            $result = Json::decode($response->getContent(), true);
            $this->setPayment($user_id,$amount,self::PAYMENT_TYPE,$token,json_encode($result));
            if ($result['Success']) {
                return \Yii::$app->controller->redirect($result['PaymentURL']);
            }
        } else {
            throw new \Exception('Unknown error occured!');
        }
    }

    private function setPayment($user_id, $amount, $payment_type, $token, $payment_id = null)
    {
        $paymentModel = new Payments();
        $paymentModel->payment_token = $token;
        $paymentModel->user_id = $user_id;
        $paymentModel->payment_amount = $amount;
        $paymentModel->payment_type = $payment_type;
        $paymentModel->payment_done = false;
        if ($payment_id) {
            $paymentModel->payment_id = $payment_id;
        }
        $paymentModel->save(false);
    }
}
